// #include <cstdio>
#include <cstdlib>
#include <iostream>
#include "sqlite3.h"
#include <string>

/*static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
	int i;
	for(i=0; i<argc; i++)
	{
		//printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
		printf("%s\n", argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}*/

using namespace std;

int main(int argc, char **argv)
{
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	int rowSelect;
	int rowCount = 0; //row number
	const unsigned char * text; //storing output

	string query = "SELECT sender, receiver, content FROM new_sms_download LIMIT 17";

	sqlite3_stmt *selectStmt;

	rc = sqlite3_open("../../sqlite_test/smscorpus.db", &db); //open database connection as db
	if (rc)
	{
		cerr << "Can't open database: " << sqlite3_errmsg(db);
		sqlite3_close(db); //close db
		return 1;
	}
	// rc = sqlite3_exec(db, query.data(), callback, 0, &zErrMsg);
	if (sqlite3_prepare(db, query.data(), query.length() + 1, &selectStmt, NULL) != SQLITE_OK)
	{
		cerr << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	rowSelect = sqlite3_step(selectStmt); //get next row
	while (rowSelect != SQLITE_DONE)
	{
		if (rowSelect == SQLITE_ROW)
		{
			// bytes = sqlite3_column_bytes(selectStmt, 2); //column 2
			text = sqlite3_column_text(selectStmt, 2); //column 2 (data)
			cout << rowCount << ": " << text << endl;
			rowCount++;
		}
		else
		{
			cerr << "Failed." << endl;
			return 1;
		}
		rowSelect = sqlite3_step(selectStmt); //step again
	}
	sqlite3_close(db); //close database connection
	return 0;
}
