TODO LIST
---------

- Implement EVIDENCES: *****PRIORITY*****
	*Sugested* Frequency Evidences: *DON'T IMPLEMENT AS FREQUENCIES - SEE BOOLEAN BELLOW*
		- Frequency of Capitals/Frequency of Capitalized Sentences
		- Sentences per message.
		- # of spaces after period.
		- Count of Single quotes/ Count of double quotes
		- Frequency of emoticons/internet slang (k vs ok, lol, haha)
		- Frequency of specific emoticons ex :) vs 8-D 
		- Frequency of hits on a word list. 
	BOOLEAN EVIDENCES *IMPLEMENT THESE FIRST*:
		- We can implement the above as booleans
		- We should do this first since the bayes weather works on booleans
		- We can go back and add frequency based crap if we have time. 

	If you have other ideas for evidence, go ahead and implement your own!
	Make sure you document what the heck the evidence is in the code with a comment or two.  

- Message table in evidences.db needs to be filled up with the corpus data (Need evidences first)

- General purpose class for implementing evidences?? DON'T BOTHER WITH THIS UNLESS WE ACTUALLY HAVE IMPLEMENTATIONS FOR EVIDENCES

- Decision Logic -> Adapting weather to generalized evidences. 

- GUI hook 

