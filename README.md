TO BUILD:

Make a new subdirectory, say, "build", and type:

	 cmake ..

within this directory.  Then type:

	 make

and finally, an executable "test" should appear

It is advised to do out of source builds wherever possible (like this).
Do not check your "build" subdirectory into git!