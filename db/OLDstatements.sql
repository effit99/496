-- TODO: Force unique keys ... oops ...
-- This file will contain the schema of the database as create statements. 

CREATE TABLE evidence_master
( id INTEGER NOT NULL,
	eid INTEGER NOT NULL,
	number INTEGER,
	string TEXT,
	boolean INTEGER,
	CONSTRAINT id_pk PRIMARY KEY (id),
	CONSTRAINT eid_fk FOREIGN KEY (eid) REFERENCES evidences(eid)
);
-- important to note: id does NOT autoincrement in SQLite --
-- also important to note: this has been taken care of in the code --
CREATE TABLE evidences
(
	eid INTEGER NOT NULL,
	evidence_name TEXT,
	CONSTRAINT eid_pk PRIMARY KEY (eid)
);

CREATE TABLE messages
(
	id INTEGER NOT NULL,
	person INTEGER NOT NULL,
	message TEXT,
	eid INTEGER,
	CONSTRAINT mid_pk PRIMARY KEY (id),
	CONSTRAINT eid_fk FOREIGN KEY (eid) REFERENCES evidences(eid)
);

-- SAMPLE SELECT QUERY (implicitly joins all tables on eid)
-- select evidence_master.eid, evidence_name, person, message, number, string, boolean
-- from evidence_master, evidences, messages
-- WHERE evidence_master.eid = evidences.eid AND evidences.eid = messages.eid;
--
--SAMPLE INSERT QUERY
-- INSERT INTO evidence_master(id, eid, number, string, boolean) VALUES (2, 2, 4, null, null);


-- THIS IS OLD AND NOT NECESSARY
-- CREATE TABLE evidence_master_old
-- ( id INTEGER NOT NULL,
-- 	eid INTEGER NOT NULL,
-- 	number INTEGER,
-- 	string TEXT,
-- 	boolean INTEGER,
-- 	CONSTRAINT id_pk PRIMARY KEY (id),
-- 	CONSTRAINT eid_fk FOREIGN KEY (eid) REFERENCES evidences(eid)
-- );
