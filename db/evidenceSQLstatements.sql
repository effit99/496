-- This file will contain the schema of the database as create statements. 

CREATE TABLE evidence_master
( id INTEGER NOT NULL,
	person_id TEXT NOT NULL,
	message TEXT,
	eid INTEGER NOT NULL,
	frequency INTEGER,
	string TEXT,
	CONSTRAINT id_pk PRIMARY KEY (id),
	CONSTRAINT eid_fk FOREIGN KEY (eid) REFERENCES evidences(eid)
);

CREATE TABLE evidences
(
	eid INTEGER NOT NULL,
	evidence_name TEXT,
	CONSTRAINT eid_pk PRIMARY KEY (eid)
);

-- important to note: id does NOT autoincrement in SQLite --
-- also important to note: this has been taken care of in the code --


-- SAMPLE SELECT QUERY (implicitly joins all tables on eid)
-- select evidence_master.eid, evidence_name, person_id, message, number, string, boolean
-- from evidence_master, evidences
-- WHERE evidence_master.eid = evidences.eid
--
--SAMPLE INSERT QUERY
-- INSERT INTO evidence_master(id, person_id, message, eid, number, string) VALUES (1, 2, "message", 4, 7, null);

