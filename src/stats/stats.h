#ifndef STATS_H
#define STATS_H

#include "decision.h"
#include "evidence.h"
#include "sqlitedb.h"
#include <tuple>
#include <unordered_set>
#include <vector>

//This file contains a series of tests for evaluating the
//accuracy of our evidences

//Message is a (name,message) pair.
using Message = std::tuple<int,std::string,std::string>;
using MessageList = std::vector<Message>;
using AuthorList = std::vector<std::string>;

class Test
{
    friend class OddMessageOut;
    Test();
    Test(std::string test_name);
    ~Test();
    virtual void beginTest() = 0;

private:
    int author_count;
    int message_count;
    std::string test_name;
	MessageList messages;
    AuthorList authors;
	std::unique_ptr<EvidenceDB> dbHandle;
};

//OddMessage out goes through the message database and tests accuracy
//of each message under the evidences.
class OddMessageOut : Test
{
public:
    OddMessageOut();
    ~OddMessageOut();
    virtual void beginTest();

    void evidenceTest(e::Evidence e);
};

#endif // STATS_H
