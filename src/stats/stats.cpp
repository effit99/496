#include "stats.h"
#include "evidence.h"
#include "parser.h"
#include <iostream>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <limits>
#include <future>

Test::Test() : Test("blah")
{

}

Test::~Test()
{

}

Test::Test(std::string test_name)
{
    this->test_name = test_name;
	std::cout << "Loading corpus" << std::endl;

	SQLiteParser parser("../../../db/limit50.db");
	//LogParser parser("../../../irc1.txt");
	dbHandle = parser.parseFile();

    std::cout << "Database Parsed and loaded. " << parser.getFilename() << std::endl;

	std::cout << "Constructing message list. " << std::endl;

	SQLiteStmt stmt = dbHandle->statement("SELECT id, name, message FROM evidence");

	while (stmt.step() == SQLITE_ROW)
	{
		messages.push_back(Message(stmt.column<int>(0),stmt.column<std::string>(1),stmt.column<std::string>(2)));
	}

	authors = dbHandle->getAuthorList();

	author_count = authors.size();
	message_count = messages.size();
    std::cout << "Message & Author lists constructed. " << std::endl;

}

OddMessageOut::OddMessageOut() : Test("OddMessageOut")
{

}

OddMessageOut::~OddMessageOut()
{
    std::cout << test_name << " testing concluded." << std::endl;
}

using ThreadResult = std::tuple<int,int,int>;

ThreadResult threadFunc(int threadID, int msgCount, std::unique_ptr<EvidenceDB> dbHandle, const MessageList& messages) {

	int startIndex = threadID*msgCount;

	int pass = 0;
	int fail = 0;
	int rank_error = 0;

	auto decider = std::unique_ptr<Decision>(new kNNDecision(*dbHandle));
	//auto decider = std::unique_ptr<Decision>(new BayesianDecision(*dbHandle));

	for (int i = startIndex; i < startIndex + msgCount; i++) {
		int id;
		std::string author_name;
		std::string author_message;
		std::tie(id,author_name,author_message) = messages[i];

		//std::cout << "Removing message" << std::endl;

		//Obtain message
		auto getMsgStmt = dbHandle->statement("SELECT * FROM evidence WHERE id = ?");
		getMsgStmt.bind(id,1);
		getMsgStmt.step();

		//Save row
		std::array<double,e::func_array_size> rowVals;
		using size_type = e::func_array_type::size_type;

		for (size_type i = 0; i < e::func_array_size; i++) {
			rowVals[i] = getMsgStmt.column<double>(i+3);
		}

		//Remove message
		auto removeStmt = dbHandle->statement("DELETE FROM evidence WHERE id = ?");
		removeStmt.bind(id,1);
		removeStmt.step();

		//Redo decider calculations
		decider->flushCache();

		auto ranking = decider->adviseAllAuthors(author_message);

		for (auto& author : ranking) {
			if (author_name == std::get<0>(author)) {
				break;
			} else {
				rank_error++;
			}
		}

		if (std::get<0>(ranking.front()) == author_name) {
			pass++;
		} else {
			fail++;
		}

		//set_counter++;
		if (threadID == 0) {
			std::cout << "PROGRESS: " << (((double)i - startIndex)/msgCount) * 100 << "%" << std::endl;
		}
		//std::cout << "Reintroducing message" << std::endl;

		//Reintroduce message
		std::stringstream s;
		s << "INSERT INTO evidence VALUES (?, ?, ?, ";

		for (size_type i = 0; i < e::func_array_size - 1; i++)
		{
			s << "?, ";
		}
		s << "? )";


		auto insertStmt = dbHandle->statement(s.str());
		insertStmt.bind(id,1);
		insertStmt.bind(author_name,2);
		insertStmt.bind(author_message,3);
		for (size_type i = 0; i < e::func_array_size; i++) {
			insertStmt.bind(rowVals[i],i+4);
		}

		insertStmt.step();
	}

	return std::make_tuple(pass,fail,rank_error);

}

void OddMessageOut::beginTest()
{

    std::cout << "Beginning " << test_name << "." << std::endl;
    std::cout << "Computing accuracy for current decision model and dataset." << std::endl;

    int pass = 0;
    int fail = 0;

    int rank_error = 0;

	constexpr int numThreads = 8;

	int msgsPerThread = message_count/numThreads;
	int remainderMsgs = message_count%numThreads;

	std::future<ThreadResult> threads[numThreads];
	ThreadResult res[numThreads + 1];

	for (int i = 0; i < numThreads; i++) {
		std::unique_ptr<EvidenceDB> newDb = std::unique_ptr<EvidenceDB>(new EvidenceDB(*dbHandle));
		threads[i] = std::async(std::launch::async,threadFunc, i, msgsPerThread, std::move(newDb), messages);
	}

	for (int i = 0; i < numThreads; i++) {
		res[i] = threads[i].get();
	}

	//Do remainder
	res[numThreads] = threadFunc(numThreads, remainderMsgs, std::move(dbHandle), messages);

	for (int i = 0; i < numThreads + 1; i++){
		pass += std::get<0>(res[i]);
		fail += std::get<1>(res[i]);
		rank_error += std::get<2>(res[i]);
	}

    std::cout << "Pass: " << pass << std::endl;
    std::cout << "Fail: " << fail << std::endl;
    std::cout << "Authors: " << authors.size() << std::endl;
    std::cout << "Accuracy: " << pass / (double)(fail + pass) * 100 << "%" << std::endl;
	std::cout << "Mean Rank Error: " << rank_error / (double)message_count << std::endl;
}

void OddMessageOut::evidenceTest(e::Evidence e)
{
    //Turn off other evidences.
    for(e::Evidence f : e::functions)
    {
        (f.name == e.name) ? e::set_evidence(f,true) : e::set_evidence(f,false);
    }

    std::cout << "Beginning " << test_name << " test for evidence '" << e.name << "'" << std::endl;
    beginTest();
    //std::cout << "The above statistics are for evidence '" << e.name << "'" << std::endl;

    //Turn them back on.
    for(e::Evidence f : e::functions)
    {
        f.enabled = true;
    }
}

int main()
{
	std::cout.sync_with_stdio(false);
	e::init();
    OddMessageOut tests;
	tests.beginTest();

	//tests.priori_only();
    /*
    //Test the accuracy of all our different evidences.
    for(e::Evidence e : e::functions)
    {
        tests.evidenceTest(e);
    }
    */

    return 0;
}


