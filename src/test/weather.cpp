#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include "sqlite3.h"
#include <map>
#include <sstream>
#include <vector>
#include <tuple>
#include <iomanip>

using namespace std;

using NBPlay = tuple<string, int, int>;

//Given a condition, what is its correlation to PLAY? Yes or No
//This structure stores that information. 

struct hypothesis
{
	int yes;
	int no;
	double yes_ratio;
	double no_ratio;
};

//TABLE: EVIDENCE X HYPOTHESIS
map<string, hypothesis> eh_table;

int build_cross_play_table(void *tablePtr, int argc, char **argv, char **azColName)
{
	vector<NBPlay> *table = reinterpret_cast<vector<NBPlay> *>(tablePtr);
	
	table->emplace_back(argv[0], atoi(argv[1]), atoi(argv[2]));

	return 0;
}

//Construct a query to get a table in the format
//| col | count of play == true | count of play == false |
string construct_query(string col){
	stringstream s;
	
	s << "select " << col <<", sum(trueval) as 'True', sum(falseval) as 'False' from\n\
		(select " << col <<",\n\
	 			case when play='no' then 1 else 0 end as 'falseval',\n\
	 			case when play='yes' then 1 else 0 end as 'trueval'\n\
	 		from weather)\n\
	 	group by " << col << ";\n";
	
	return s.str();
}

int main()
{
	sqlite3 *db;
	char *zErrMsg = 0;
	string db_name = "../../../src/test/weather.db";

	if (sqlite3_open(db_name.c_str(), &db))
	{
		cerr << "Error: Can't open database: " << db_name << endl;
		sqlite3_close(db);
		return (1);
	}
	
	vector<NBPlay> play_freq;

	if (sqlite3_exec(db, construct_query("play").c_str(), build_cross_play_table, (void *) &play_freq, &zErrMsg) != SQLITE_OK)
	{
		cerr << "Error: SQL Error: " << std::string(zErrMsg) << endl;
		sqlite3_free(zErrMsg);
		return 0;
	}
	
	
	hypothesis play;
	
	//Bit of a hack, but works for now (no guarantee of order, I don't think)
	play.no = get<2>(play_freq[0]);
	play.yes = get<1>(play_freq[1]);
	int play_sum = play.no + play.yes;
	play.no_ratio = (double)play.no/(double)play_sum;
	play.yes_ratio = (double)play.yes/(double)play_sum;
	
	cout << "play:" << endl;
	cout << setw(10) << "yes:" << setw(5) << play.yes << setw(8) << setprecision(2)
			<< play.yes_ratio << endl;
	
	cout << setw(10) << "no:" << setw(5) << play.no << setw(8) << setprecision(2)
			<< play.no_ratio << endl << endl;
	
	vector<string> column_name = {"outlook", "temperature", "humidity", "windy"};

	for (auto &col : column_name)
	{
		vector<NBPlay> nb_freq;
		string query = construct_query(col);
		
		//cout << "Query dump: " << query << endl;

		if (sqlite3_exec(db, query.c_str(), build_cross_play_table, (void *) &nb_freq, &zErrMsg) != SQLITE_OK)
		{
			cerr << "Error: SQL Error: " << std::string(zErrMsg) << endl;
			sqlite3_free(zErrMsg);
			return 0;
		}

		for (NBPlay &r : nb_freq)
		{	
			hypothesis h;
			string name;
			
			tie(name,h.yes,h.no) = r;

			h.no_ratio = (double)h.no/(double)play.no;
			h.yes_ratio = (double)h.yes/(double)play.yes;
			
			eh_table.insert({name,h});
		}

	}
	
		
	for (auto &entry : eh_table){
		
		hypothesis &h = entry.second;
		
		cout << setw(10) << entry.first << setw(5)  << h.yes << setw(5) << h.no
			<< setw(10) << setprecision(2) << h.yes_ratio << setw(5) << setprecision(2)
			<< h.no_ratio<< endl;
	}
	
	
	double sunny = eh_table["sunny"].no_ratio;
	double cool = eh_table["cool"].no_ratio;
	double high = eh_table["high"].no_ratio;
	double _true = eh_table["TRUE"].no_ratio;
	double likely_no = sunny*cool*high*_true*play.no_ratio;
	
	sunny = eh_table["sunny"].yes_ratio;
	cool = eh_table["cool"].yes_ratio;
	high = eh_table["high"].yes_ratio;
	_true = eh_table["TRUE"].yes_ratio;
	
	double likely_yes = sunny*cool*high*_true*play.yes_ratio;
	
	
	cout << "P(play=yes | outlook=sunny, temp=cool, humidity=high, windy=TRUE)" << endl;
	cout << "\t=P(outlook=sunny | play=yes) (" << sunny << ")" << endl;
	cout << "\t*P(temp=cool | play=yes) (" << cool<< ")" << endl;
	cout << "\t*P(humidity=high | play=yes) (" << high << ")" << endl;
	cout << "\t*P(windy=TRUE | play=yes) (" << _true << ")" << endl;
	cout << "\t*P(play=yes) (" << play.yes_ratio << ")" << endl;
	cout << "\t\\P(outlook=sunny, temp=cool, humidity=high, windy=TRUE) (" << likely_yes << " + " << setprecision(3) << likely_no << ")" << endl;
	cout << "\t=" << setprecision(3) << likely_yes/(likely_yes+likely_no) << endl;

	return 0;
}


