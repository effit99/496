#include "evidence.h"
#include <iostream>
#include <string>

using namespace std;
using namespace e;


class evidence_test
{
	int (*f)(const string&);
	const string module_name;
	bool pass = true;
	int count = 0;

public:

	evidence_test(int (*ref)(const string&), const string& m) : f{ref}, module_name{m}
	{
		cout << "Testing '" << module_name << "'" << endl;
	}

	void test(string message, int expected_return)
	{
		cout << "Test " << ++count << endl;
		cout << "-----" << "Message: '" << message << "'" << endl;
		cout << "-----" << "Expected Return: " << expected_return << endl;
		int ret = f(message);

		cout << "-----" << "Actual Return: " << ret << endl << "-----";
		if( pass&=(ret == expected_return) )
			cout << "PASS" << endl;
		else
			cout << "FAIL" << endl;
	}

	void result()
	{
		cout << "The evidence '" << module_name << "' has " << ((pass) ? " PASSED" : "FAILED") << endl;
	}
};


int main()
{

	init();
	cout << "Evidence Testing" << endl;
	cout << "----------------" << endl;

	evidence_test e1(emoticons,"emoticons");

	e1.test("Hello! How are you?",0);
	e1.test("Heyo :D",1);
	e1.test("Smiley! :)",1);
    e1.test("Multiple smileys :D :D :D",3);

	evidence_test e2(punctuation,"punctuation");
	e2.test("this sentence does not contain punctuation",0);
	e2.test("This sentence totally contains punctuation!",1);

	evidence_test e3(first_capital_sentence,"first_capital_sentence");
	e3.test("this is a sentence which does not begin with a capital. However this one does!",1);
	e3.test("this sentence doesn't have caps. this one does But its int the middle!",0);

	evidence_test e4(period_space,"period_space");
	e4.test("this is a period with a space. this is another sentence",1);
	e4.test("this is a period with no space.and anothersentence",0);

	evidence_test e5(period_double_space,"period_double_space");
	e5.test("this is a setnence with two spaces after the period.  this is a sentence",1);
	e5.test("this is a sentence without two spaces after the period. and another sentence",0);

	evidence_test e6(first_capital_message, "first_capital_message");
	e6.test("The first letter of this sentence is capitalized.", 1);
	e6.test("TheRe are MANY capitals in this SENTENCE.  This one too.", 1);
	e6.test("sOMEONE LEFT THE CAPS LOCK KEY ON!!!11111",0);

    evidence_test e7(word_count, "word_count");
    e7.test("ther are 7 words in this message !", 7);

    evidence_test e8(avg_word_length, "avg_word_length");
    e8.test("the avg wrd lng for ths msg iss thr eee", 3);
    e8.test("the avg wrd lng for ths msg iss also thr eee",3);

    evidence_test e9(length_chars, "length_chars");
    e9.test("there are 29 chars in this message!", 29);

    evidence_test e10(length_words, "length_words");
    e10.test("there are 5 words here", 5);

    evidence_test e11(distinct_word_count,"distinct_word_count");
    e11.test("there are 5 distinct words", 5);
    e11.test(" there are 5 distinct words words ",5);

    evidence_test e12(distinct_char_count,"distinct_char_count");
    e12.test("there are 16 unique-chars in this message! 16",16);

    evidence_test e13(first_person_pronoun_count,"first_person_pronoun_count");
    e13.test("i have placed several of my personal pronouns in mY message here.",3);

    evidence_test e14(second_person_pronoun_count,"second_person_pronoun_count");
    e14.test("we have place many of our second person pronouns in this message",2);

    evidence_test e15(third_person_pronoun_count,"third_person_pronoun_count");
    e15.test("They think that they're crafty with their pronouns in this message ~ tin foil hat man",3);

    evidence_test e16(interogative_count,"interogative_count");
    e16.test("wh0 is that, what do you want with me?",2);

    evidence_test e17(negative_count,"negative_count");
    e17.test("no I don't like what you won't say never",4);

    evidence_test e18(positive_count,"positive_count");
    e18.test("ya I like this message", 2);

    evidence_test e19(apologetic_count,"apologetic_count");
    e19.test("I'm sry",1);

    evidence_test e20(capital_count,"capital_count");
    e20.test("UIOPQ",5);

    evidence_test e21(lowercase_count,"lower_case_count");
    e21.test("abcdef",6);

    evidence_test e22(exclamation_count,"exclamation_count");
    e22.test("!!!",3);

    evidence_test e23(question_mark_count,"question_mark_count");
    e23.test("???",3);

    evidence_test e24(comma_count,"comma_count");
    e24.test(",,,,",4);

    evidence_test e25(vowel_count,"vowel_count");
    e25.test("abcdefg", 2);

    evidence_test e26(consonant_count,"consonant_count");
    e26.test("abcdefg",5);

    evidence_test e27(number_count,"number_count");
    e27.test("123",3);

    evidence_test e28(avg_period_space, "avg_period_space");
    e28.test("This sentence has 2 periods afterwards.  This sentence also has 2 periods afterwards.  ", 2);
    e28.test("This sentence does not have any spaces after period.This sentence has 2 spaces after the period.  ", 1);
    e28.test("This sentence has one space after the period. This sentence has two spaces after the period.  ", 1);

	e1.result();
	e2.result();
	e3.result();
	e4.result();
	e5.result();
	e6.result();
    e7.result();
    e8.result();
    e9.result();
    e10.result();
    e11.result();
    e12.result();
    e13.result();
    e14.result();
    e15.result();
    e16.result();
    e17.result();
    e18.result();
    e19.result();
    e20.result();
    e21.result();
    e22.result();
    e23.result();
    e24.result();
    e25.result();
    e26.result();
    e27.result();
    e28.result();

	return 0;
}
