#include "chatsafe.h"
#include "debugger.h"
#include "decision.h"
#include "evidence.h"
#include "parser.h"
#include "sqlitedb.h"
#include "ui_chatsafe.h"
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <QFileDialog>

ChatSafe::ChatSafe(QWidget *parent) :
    QMainWindow(parent),
	ui(new Ui::ChatSafe),
	decider(nullptr)
{
    ui->setupUi(this);
	ui->rankingView->setModel(&model);
    show();
}

ChatSafe::~ChatSafe()
{
    delete ui;
}

void ChatSafe::setStatus(const QString &s){
	ui->statusbar->showMessage(s);
}

auto ChatSafe::getDecision() -> const std::unique_ptr<BayesianDecision>&
{
	return decider;
}

int main(int argc, char *argv[]){

	e::init();
	std::iostream::sync_with_stdio(false);

	QApplication app(argc, argv, 5);

	ChatSafe s;

	s.show();

	return app.exec();
}


void ChatSafe::on_actionTrainLog_triggered()
{
	QString file_name = QFileDialog::getOpenFileName(this);

	if (file_name.isEmpty())
		return;

	LogParser parser(file_name.toStdString());

	trainDecider(static_cast<Parser&>(parser));

}

void ChatSafe::on_actionExit_triggered()
{
	exit(EXIT_SUCCESS);
}

void ChatSafe::on_actionTrainSMS_triggered()
{
    /*QString file_name = QFileDialog::getOpenFileName(this);
	 *	if (file_name.isEmpty())
		return;
    SQLiteParser parser(file_name.toStdString());
    */
	SQLiteParser parser("../../../db/limit50.db");
    setStatus(QString::fromStdString(parser.getDescription()) + " loading corpus: " + QString::fromStdString(parser.getFilename()));
	trainDecider(parser);
}

void ChatSafe::trainDecider(Parser &parser) {
	db = parser.parseFile();
	std::string file_name = parser.getFilename();

	QFileInfo file_info(QString::fromStdString(file_name));

	decider = std::unique_ptr<BayesianDecision>(new BayesianDecision(*db));
	auto authors = decider->getAuthorList();

    ui->comboBox->clear();
    QStringList list;
    for (auto& author : authors){
        list.append(QString::fromStdString(author));
	}

	emit newTrainingSet();
    ui->comboBox->addItems(list);

    setStatus(QString::fromStdString(parser.getDescription()) + " loaded: " + file_info.fileName());

}

void ChatSafe::on_plainTextEdit_textChanged()
{

	if (decider != nullptr){
		QString newStatus = QLatin1String("Selected author probability: %1%");
		auto ranking = decider->adviseAllAuthors(ui->plainTextEdit->toPlainText().toStdString());

		double r = -1;

		for (auto& author : ranking) {
			if (ui->comboBox->currentText().toStdString() == std::get<0>(author)) {
				r = std::get<1>(author);
				break;
			}
		}

		if (r == -1) {
			throw std::runtime_error("Error: Author does not exist in ranking list");
		}

		ui->selectedAuthor->setText(newStatus.arg(r*100));
		std::string authorName;
		double authorProb;
		std::tie(authorName,authorProb) = ranking.front();
		QString newPredicted = QLatin1String("Predicted author: %1 Probability: %2%");
		ui->predictedAuthor->setText(newPredicted.arg(QString::fromStdString(authorName)).arg(authorProb*100));

		rankingList.clear();
		for (auto& author: ranking) {
			rankingList.push_back(QString(QLatin1String("%1: %2%")).arg(QString::fromStdString(std::get<0>(author))).arg(std::get<1>(author)*100));
		}
		model.setStringList(rankingList);

		emit result();
	}
}

void ChatSafe::on_comboBox_currentTextChanged(const QString &arg1)
{
	if (!arg1.isEmpty())
		on_plainTextEdit_textChanged();
}

void ChatSafe::on_actionDebug_triggered()
{

	for (const auto& d : debuggers)
	{
		if (d->isHidden())
		{
			d->show();
			return;
		}
	}

	//For exception safety
	debuggers.emplace_back(std::unique_ptr<Debugger>{new Debugger{this}});

	const std::unique_ptr<Debugger>& d = debuggers.back();

	connect(this,
			SIGNAL(newTrainingSet()),
			d.get(),
			SLOT(updateModel()),
			Qt::ConnectionType::UniqueConnection);

	connect(this,
			SIGNAL(result()),
			d.get(),
			SLOT(updateResults()),
			Qt::ConnectionType::UniqueConnection);

	const QRect& windowGeometry = geometry();

	d->move(windowGeometry.x() + windowGeometry.width() + 50, windowGeometry.y() - 50);
	d->show();

}
