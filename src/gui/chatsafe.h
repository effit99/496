#ifndef CHATSAFE_H
#define CHATSAFE_H

#include "debugger.h"
#include "decision.h"
#include "parser.h"
#include <memory>
#include <QMainWindow>
#include <QStringListModel>

namespace Ui {
class ChatSafe;
}

class ChatSafe : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ChatSafe(QWidget *parent = 0);
    ~ChatSafe();
    void setStatus(const QString &s);
	auto getDecision() -> const std::unique_ptr<BayesianDecision>&;
    
signals:
	void newTrainingSet();
	void result();


private slots:
	void on_actionTrainLog_triggered();
	void on_actionExit_triggered();
	void on_actionTrainSMS_triggered();
	void on_plainTextEdit_textChanged();
	void on_comboBox_currentTextChanged(const QString &arg1);
	void on_actionDebug_triggered();

private:
    Ui::ChatSafe *ui;
	std::unique_ptr<EvidenceDB> db;
	std::unique_ptr<BayesianDecision> decider;
	std::vector<std::unique_ptr<Debugger>> debuggers;
	void trainDecider(Parser &parser);
	QStringList rankingList;
	QStringListModel model;

};

#endif // CHATSAFE_H
