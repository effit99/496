#ifndef DEBUGGER_H
#define DEBUGGER_H

#include "decision.h"
#include "evidence.h"
#include "qcustomplot.h"
#include <memory>
#include <QDialog>

namespace Ui {
class Debugger;
}

class ChatSafe;

class Debugger : public QDialog
{
	Q_OBJECT
	
public:
	explicit Debugger(ChatSafe *parent);
	~Debugger();
	void updateAuthorData(const QString& author, const int graph_id);

public slots:
	void updateModel();
	void updatePlotAuthor1();
	void updatePlotAuthor2();
	void updatePlotEvidence();
	void updateResults();

private slots:
	void on_evidenceTable_clicked(const QModelIndex &index);

private:

	Ui::Debugger *ui;
	ChatSafe *mainWindow;
	QCustomPlot *plot;
	QCPItemStraightLine *line;
	std::size_t evidence;
};

#endif // DEBUGGER_H
