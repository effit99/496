#include "chatsafe.h"
#include "evidence.h"
#include "decision.h"
#include "debugger.h"
#include "qcustomplot.h"
#include "ui_debugger.h"
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>
#include <QClipboard>

Debugger::Debugger(ChatSafe* parent) :
	QDialog{parent},
	ui{new Ui::Debugger},
	mainWindow{parent}
{
	ui->setupUi(this);

	for (auto ev : e::functions)
	{
		std::stringstream name;
		name << ev.index << ". " << ev.name;
		ui->evidenceSelector->addItem(QString::fromStdString(name.str()));
	}

	plot = ui->plot;

	plot->legend->setVisible(true);
	QFont legendFont = font();  // start out with MainWindow's font..
	legendFont.setPointSize(9); // and make a bit smaller for legend
	plot->legend->setFont(legendFont);
	plot->legend->setPositionStyle(QCPLegend::psTopRight);
	plot->legend->setBrush(QBrush(QColor(255,255,255,230)));
	plot->legend->setAntialiased(true);

	plot->addGraph();
	plot->graph(0)->setName("Author 1");

	QPen author1Pen{Qt::red};
	//author1Pen.setWidth(2);
	plot->graph(0)->setPen(author1Pen);
	plot->graph(0)->setLineStyle(QCPGraph::lsLine);
	plot->graph(0)->setAntialiased(true);
	plot->addGraph();
	plot->graph(1)->setName("Author 2");

	QPen author2Pen{Qt::blue};
	//author2Pen.setWidth(2);
	plot->graph(1)->setPen(author2Pen);
	plot->graph(1)->setLineStyle(QCPGraph::lsLine);
	plot->graph(1)->setAntialiased(true);

	plot->xAxis->setLabel(QLatin1String("Frequency"));
	plot->yAxis->setLabel(QLatin1String("Density"));

	connect(ui->author1Selector,
			SIGNAL(currentTextChanged(QString)),
			this,
			SLOT(updatePlotAuthor1()),
			Qt::ConnectionType::UniqueConnection);

	connect(ui->author2Selector,
			SIGNAL(currentTextChanged(QString)),
			this,
			SLOT(updatePlotAuthor2()),
			Qt::ConnectionType::UniqueConnection);

	connect(ui->evidenceSelector,
			SIGNAL(currentTextChanged(QString)),
			this,
			SLOT(updatePlotEvidence()),
			Qt::ConnectionType::UniqueConnection);

	line = new QCPItemStraightLine(plot);

	QPen pen{Qt::darkGreen};
	pen.setStyle(Qt::DashLine);
	//pen.setWidth(2);
	line->setPen(pen);
	evidence = 0;

	ui->evidenceTable->clear();
	ui->evidenceTable->setRowCount(2);
	ui->evidenceTable->setColumnCount(e::func_array_size);

	QStringList e_names;
	for (auto& ev : e::functions)
	{
		e_names.append(QString::fromStdString(ev.name));
	}
	ui->evidenceTable->setHorizontalHeaderLabels(e_names);
	ui->evidenceTable->resizeColumnsToContents();

	updateModel();
}

Debugger::~Debugger()
{
	delete ui;
}

void Debugger::updateAuthorData(const QString& author, const int graph_id)
{
	const std::unique_ptr<BayesianDecision>& decider = mainWindow->getDecision();

	if (decider == nullptr) return;

	//const QString& name = ui->author1Selector->currentText();
	if (!author.isEmpty()){
		QVector<double> x;
		QVector<double> y;

		const e::Evidence& ev = e::functions[evidence];

		//double priori = decider->cache.calc_priori(author.toStdString());

		// TEMP
		auto moment = decider->engine->calc_moment_name(
									  author.toStdString(),
									  ev);
		if (decider->engine->indeterminate(moment)) {
			x.append(-5);
			y.append(1);
			x.append(10);
			y.append(1);
		} else {
			//if (variance == 0) variance = 0.5 - priori/2;
			//std::cout << "Mean: " << mean << " Variance: " << variance <<  std::endl;

			double start;
			double end;
			std::tie(start,end) = decider->engine->getRange(moment);

			for (double i = start - 2; i < end + 2; i += 0.01)
			{
				x.append(i);
				if (ev.conditional && i < 0)
					y.append(1);
				else
					y.append(decider->engine->probability(moment,i));
			}
		}
		plot->graph(graph_id)->setData(x,y);
		plot->rescaleAxes();

		plot->graph(graph_id)->setName(author);

		QTableWidgetItem* header = new QTableWidgetItem(author);
		ui->evidenceTable->setVerticalHeaderItem(graph_id,header);

		for (auto& ev : e::functions){
			double stat = decider->lastStats[ev.index];
			auto moment = decider->engine->calc_moment_name(author.toStdString(), ev);
			if (decider->engine->indeterminate(moment)) {
				ui->evidenceTable->setItem(graph_id,
										   ev.index, new QTableWidgetItem(QLatin1String("1")));
			} else {
				//if (variance == 0) variance = 0.5 - priori/2;
				double prob = decider->engine->probability(moment,stat);
				ui->evidenceTable->setItem(graph_id,
										   ev.index,
										   new QTableWidgetItem(QString(QLatin1String("%1")).arg(prob)));
			}
		}
	}

}

void Debugger::updateModel()
{
	const std::unique_ptr<BayesianDecision>& decider = mainWindow->getDecision();

	if (decider == nullptr) return;

	ui->author1Selector->clear();
	ui->author2Selector->clear();

	const std::vector<std::string>& names = decider->getAuthorList();

	for (const std::string& name : names)
	{
		QString convert = QString::fromStdString(name);
		ui->author1Selector->addItem(convert);
		ui->author2Selector->addItem(convert);
	}

	updateResults();

}

void Debugger::updatePlotAuthor1()
{

	const QString& name = ui->author1Selector->currentText();
	updateAuthorData(name, 0);
	plot->replot();

}


void Debugger::updatePlotAuthor2()
{
	const QString& name = ui->author2Selector->currentText();
	updateAuthorData(name, 1);
	plot->replot();
}

void Debugger::updatePlotEvidence()
{

	evidence = ui->evidenceSelector->currentIndex();

	updateResults();
}

void Debugger::updateResults()
{
	const std::unique_ptr<BayesianDecision>& decider = mainWindow->getDecision();

	// TEMP
	if (decider == nullptr) return;

	const std::array<double, e::func_array_size>& stats = decider->lastStats;

	double stat = stats[evidence];

	auto* point1 = line->position(QLatin1String("point1"));
	auto* point2 = line->position(QLatin1String("point2"));

	point1->setCoords(stat,6);
	point2->setCoords(stat,5);

	const QString& name1 = ui->author1Selector->currentText();
	const QString& name2 = ui->author2Selector->currentText();
	updateAuthorData(name1,0);
	updateAuthorData(name2,1);
	plot->replot();

}

void Debugger::on_evidenceTable_clicked(const QModelIndex &index)
{
	QString csv = ",";
	ui->evidenceTable->selectAll();
	for (int i = 0; i < ui->evidenceTable->columnCount(); i++)
	{
		QTableWidgetItem *header = ui->evidenceTable->horizontalHeaderItem(i);
		if (header){
			if (i != ui->evidenceTable->columnCount())
				csv += header->text() + QLatin1String(",");
			else
				csv += header->text();
		}
	}
	csv += QLatin1String("\r\n");

	for (int i = 0; i < ui->evidenceTable->rowCount(); i++)
	{
		csv += ui->evidenceTable->verticalHeaderItem(i)->text() + QLatin1String(",");
		for (int j = 0; j < ui->evidenceTable->columnCount(); j++)
		{
			QTableWidgetItem* item = ui->evidenceTable->item(i,j);
			if (item)
			{
				if (j != ui->evidenceTable->columnCount()-1)
					csv += ui->evidenceTable->item(i,j)->text() + QLatin1String(",");
				else
					csv += ui->evidenceTable->item(i,j)->text();
			}
		}
		csv += QLatin1String("\r\n");
	}

	QClipboard* clip = QApplication::clipboard();
	//std::cout << csv.toStdString() << std::endl;
	clip->setText(csv);
}
