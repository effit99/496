#ifndef PARSER_H
#define PARSER_H

#include "sqlite3.h"
#include "sqlitedb.h"
#include <fstream>
#include <memory>
#include <string>
#include <vector>

//Parsers
struct Parser{

	Parser(const std::string& fileName);
	virtual auto parseFile() -> std::unique_ptr<EvidenceDB> = 0;
	virtual auto getDescription() -> std::string = 0;
	auto getFilename() -> std::string;

protected:
	std::string file_name;
};

/*
	LogParser

	Accepts a file with format:
	"name: text"
	and returns database with evidences filled out
*/
struct LogParser : Parser
{
	LogParser(const std::string& fileName);
	virtual auto parseFile() -> std::unique_ptr<EvidenceDB>;
	virtual auto getDescription() -> std::string;

private:
	std::ifstream file;
};

/*
	SQLiteParser

	Accepts an SQL database with SMS Corpus format
	 and returns new database with evidences filled out
*/
struct SQLiteParser : Parser
{
	SQLiteParser(const std::string& fileName);
	virtual auto parseFile() -> std::unique_ptr<EvidenceDB>;
	virtual auto getDescription() -> std::string;
};

#endif // PARSER_H
