#include <iostream>
#include <string>
#include "evidence.h"
#include "sqlite3.h"
#include <sstream>
#include <list>

using namespace std;

/* DO NOT MODIFY THIS CODE, CHANGES STILL TO BE MADE FOR USE IN RELEASE */

class db_entry
{
	/* message details and content */
	string personID; //senderID. receiverID is irrelevant.
	string msg; //message sent

	/* evidence integers */
	int punctuation=0;
	int emoticons=0;
	int period_space=0;
	int period_double_space=0;
	int first_capital_sentence=0;
	int first_capital_message=0;

public:

	db_entry(string newPersonID, string newMessage)
	{
		personID = newPersonID;
		msg = newMessage;
		findEvidenceFromMessage(); //no sense in not finding evidence right away
	}

	auto findEvidenceFromMessage() -> void
	{
		period_space = e::period_space(msg);
		period_double_space = e::period_double_space(msg);
		first_capital_sentence = e::first_capital_sentence(msg);
		first_capital_message = e::first_capital_message(msg);
		punctuation = e::punctuation(msg);
		emoticons = e::emoticons(msg);
	}

	auto insertIntoDatabase() -> int
	{
		//insert messages into the database with their entries
		sqlite3 *dbEvidence;
		int rc;
		int nextID;
		stringstream queryPeriodSpace, queryPeriodDoubleSpace, queryFirstCapSentence,
					 queryFirstCapMessage, queryPunctuation, queryEmoticons;


		rc = sqlite3_open("../../../db/evidences.db", &dbEvidence); //relative to build directory
		if(rc)
		{
			cerr << "Can't open database: " << sqlite3_errmsg(dbEvidence) << endl;
			sqlite3_close(dbEvidence);
			return -1; //failure
		}
		//successful open, so continue
		
		cout << "opened evidence db" << endl;

		//forcing unique IDs since sqlite won't cooperate on this
		nextID=getLastEntryId(dbEvidence);

		// the evidence table starts its IDs at 2 due to corrections that have been made

		queryPeriodSpace 
			<< buildQueryStringStream(nextID+1, personID, msg, 2, period_space);
		queryPeriodDoubleSpace 
			<< buildQueryStringStream(nextID+1, personID, msg, 3, period_double_space);
		queryFirstCapSentence 
			<< buildQueryStringStream(nextID+1, personID, msg, 4, first_capital_sentence);
		queryFirstCapMessage 
			<< buildQueryStringStream(nextID+1, personID, msg, 5, first_capital_message);
		queryPunctuation 
			<< buildQueryStringStream(nextID+1, personID, msg, 6, punctuation);
		queryEmoticons 
			<< buildQueryStringStream(nextID+1, personID, msg, 7, emoticons);

		executeQuery(dbEvidence, queryPeriodSpace.str());
		executeQuery(dbEvidence, queryPeriodDoubleSpace.str());
		executeQuery(dbEvidence, queryFirstCapSentence.str());
		executeQuery(dbEvidence, queryFirstCapMessage.str());
		executeQuery(dbEvidence, queryPunctuation.str());
		executeQuery(dbEvidence, queryEmoticons.str());
		if(sqlite3_close(dbEvidence)==SQLITE_OK)
		{
			cout << "closed evidence db" << endl;
		}
		else
		{
			cout << "failed to close" << endl;
		}

		return 0;

	}

	auto executeQuery(sqlite3 *dbToWorkWith, string query) -> void
	{
		sqlite3_stmt *preparedQuery;
		char *zErrMsg = 0;

		if(sqlite3_prepare(dbToWorkWith, 
			query.data(), 
			query.length() + 1, 
			&preparedQuery, NULL) != SQLITE_OK) //prepare the query
		{
			cerr << "\n--SQL error preparing insertion query: " << query << endl;
			sqlite3_free(zErrMsg);
		}
		else
		{
			cout << "executing this query: " << query << endl; //debug printout
			sqlite3_step(preparedQuery); //uncomment this when ready
		}
		sqlite3_finalize(preparedQuery);

	}

	auto buildQueryStringStream(int id, string p_id, string content, 
								int evidence_id, int evidence_value) -> string
	{
		stringstream builtQuery;
		builtQuery << "INSERT INTO evidence_master VALUES ("
		 << id << ", "
		 << "'" << p_id << "', "
		 << "'" << content << "', " 
		 << evidence_id << ", "
		 << evidence_value
		 << ", NULL)";

		return builtQuery.str();
	}

	auto getLastEntryId(sqlite3 *database) -> int //forces unique ID by finding one above max
	{
		string selectQuery="SELECT id FROM evidence_master"; //select all eid
		sqlite3_stmt *preparedSelectQuery;
		char *zErrMsg=0;
		int rowSelect;
		int maxID=0;
		int foundID;

		if(sqlite3_prepare(database, 
			selectQuery.data(), 
			selectQuery.length() + 1, 
			&preparedSelectQuery, NULL) != SQLITE_OK)
		{
			cerr << "SQL error preparing query: " << selectQuery << "Error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
		}
		rowSelect=sqlite3_step(preparedSelectQuery);
		while(rowSelect != SQLITE_DONE)
		{
			if(rowSelect == SQLITE_ROW)
			{
				foundID = sqlite3_column_int(preparedSelectQuery, 0); //first row AKA ID row
				if(foundID >= maxID) //find biggest ID
				{
					maxID = foundID;
				}
			}
			else
			{
				cerr << "what happened" << endl;
				return -2;
			}
			rowSelect = sqlite3_step(preparedSelectQuery);
		}
		sqlite3_finalize(preparedSelectQuery);
		return maxID;
	}

	auto getAllEvidenceValues() -> void //basically just a test function
	{
		// cout << "--- RUNNING TEST FUNCTION ---" << endl;
		cout << "Person ID is: " << personID << endl;
		cout << "Message is: " << msg << endl;
		cout << "single period space = " << period_space << endl;
		cout << "double period space = " << period_double_space << endl;
		cout << "first_capital_message = " << first_capital_message << endl;
		cout << "first_capital_sentence = " << first_capital_sentence << endl;
		cout << "punctuation = " << punctuation << endl;
		cout << "emoticons = " << emoticons << endl;
	}
};

int main()
{
	sqlite3 *dbCorpus;
	char *zErrMsg=0;
	int dbOpenFail;
	sqlite3_stmt *preparedCorpusQuery;
	int rowSelectCorpus;
	string queryCorpus="SELECT sender, content, collect_time FROM corpus";

	int insertResult=0;
	
	//for use in selecting from corpus
	const unsigned char * personID;
	const unsigned char * messageContent;

	e::init();

	dbOpenFail=sqlite3_open("../../../db/smscorpus.db", &dbCorpus);
	if(dbOpenFail)
	{
		cerr << "Can't open sms corpus: " << sqlite3_errmsg(dbCorpus);
		return -1;
	}

	if(sqlite3_prepare(dbCorpus, 
		queryCorpus.data(), 
		queryCorpus.length() + 1, 
		&preparedCorpusQuery, NULL) != SQLITE_OK)
	{
		cerr << "Failed to prepare corpus query: " << queryCorpus << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	rowSelectCorpus=sqlite3_step(preparedCorpusQuery);
	while(rowSelectCorpus != SQLITE_DONE)  //need to go through 51652 messages from corpus
	{
		if(rowSelectCorpus == SQLITE_ROW)
		{
			personID = sqlite3_column_text(preparedCorpusQuery, 0); //get sender ID
			messageContent = sqlite3_column_text(preparedCorpusQuery, 1); //get message itself
			db_entry d1(string(reinterpret_cast<const char *>(personID)), 
				string(reinterpret_cast<const char *>(messageContent)));
			insertResult = d1.insertIntoDatabase();
			if(insertResult == -1) //if insertion failed
			{
				return 0; //stop it
			}
			
		}
		rowSelectCorpus=sqlite3_step(preparedCorpusQuery);
	}
	sqlite3_finalize(preparedCorpusQuery);
	sqlite3_close(dbCorpus);

}