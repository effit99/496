#ifndef SQLITEDB_H
#define SQLITEDB_H

#include "evidence.h"
#include "sqlite3.h"
#include <exception>
#include <iterator>
#include <string>
#include <tuple>
#include <vector>

//SQLiteStmt and SQLiteDB are highly coupled
struct SQLiteDB;

//For results with many rows, convenience
//struct SQLiteStmtIterator;

//An SQLite Statement.  Must create via SQLiteDB object
struct SQLiteStmt
{
	friend class SQLiteDB;
	SQLiteStmt(const SQLiteStmt&) = delete;
	SQLiteStmt(SQLiteStmt &&);

	auto bind(int t, int i) -> void;
	auto bind(double t, int i) -> void;
	auto bind(const std::string& text, int i) -> void;
	auto step() -> int;
	auto reset() -> void;
	auto columns() -> int;
	template <typename T>
	auto column(int col) -> T;

	~SQLiteStmt();

private:
	SQLiteStmt(SQLiteDB &db, const std::string& stmt);
	SQLiteDB& database;
	sqlite3_stmt *p_stmt;
};

template<> auto SQLiteStmt::column(int col) -> int;
template<> auto SQLiteStmt::column(int col) -> std::string;
template<> auto SQLiteStmt::column(int col) -> double;

//Represents an SQLite database with a single connection, in read/write mode.
struct SQLiteDB
{
	friend class SQLiteStmt;
    friend class EvidenceDB;
	SQLiteDB(const SQLiteDB&) = delete;
	SQLiteDB(SQLiteDB&&);
	SQLiteDB(const std::string& filename);
	SQLiteDB(); //Temporary database

	auto statement(const std::string& stmt) -> SQLiteStmt;
	~SQLiteDB();

private:
	std::string fileName;
	sqlite3 *dbHandle;
};

struct SQLiteException : std::exception
{
	SQLiteException(sqlite3* db, int retval);
	auto what() const noexcept -> const char *;

	int errcode;
	std::string errstr;
	std::string errmsg;
};

//Represents an Evidence database.  If const, then database is "read only"
struct EvidenceDB : SQLiteDB
{
	EvidenceDB();
	EvidenceDB(EvidenceDB&&);
	EvidenceDB(EvidenceDB&);

	void initDB();

	using gauss_type = std::tuple<const std::string,const double,const double>;
	using names_type = const std::vector<std::string>;
	using meanvariance_type = std::tuple<double,double>;

	auto getAuthorList() -> names_type;
	auto insertMessage(const std::string& name, const std::string& message) -> void;
	//auto calc_ni(const std::string& name) -> int; //Probably not useful with recent change
	auto calc_priori(const std::string& name) -> double;

	//Returns meanvariance for all authors for a given evidence, but only the meanvariance of the
	//non-indeterminate rows.  If all rows are indeterminate, then that author will not appear in the output.
	auto calc_gaussian(const e::Evidence& ev)
	-> const std::vector<gauss_type>;

	//For custom classifier
	auto get_evidence_values(const std::string& name, const e::Evidence& ev) -> std::vector<double>;

	/*auto calc_meanvariance_name(const std::string& name, const e::Evidence &ev)
	-> meanvariance_type;*/

	/*Return an SQLiteStmt that can be stepped through to obtain mean and variance
	for every author for every evidence.
	Results of form:
		   |    evidence0    |     evidence1   |
	| Name | mean | variance | mean | variance | ...
	*/
	//auto calc_meanvariance() -> SQLiteStmt;

};

#endif // SQLITEDB_H
