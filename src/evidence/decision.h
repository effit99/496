#ifndef DECISION_H
#define DECISION_H
#include "evidence.h"
#include "parser.h"
#include "sqlitedb.h"
#include <cmath>
#include <memory>
#include <string>
#include <unordered_map>
#include <map>
#include <set>

template <typename T>
struct moment;

struct ModifiedGaussian;
struct InterpolatedHistogram;

template<>
struct moment<ModifiedGaussian> {
	using type = EvidenceDB::meanvariance_type;
};

//Safe because the keys will not be determined as the result of computation
//only as simply what's been retrieved from the database
//TODO: Add a threshold for equality
template<>
struct moment<InterpolatedHistogram> {
	using type = std::map<double,int>;
};


template <typename T>
struct BayesianDecisionEngine {
	BayesianDecisionEngine(EvidenceDB& db) : database{db} {}

	auto calc_priori(const std::string& author) -> double
	{
		auto it = priori_cache.find(author);

		double priori;

		if( it != priori_cache.cend() ) {
			priori = it->second;
		}
		else
		{
			priori = database.calc_priori(author);
			priori_cache[author] = priori;
		}

		return priori;

	}

	auto calc_moment_name(const std::string& author, const e::Evidence& ev) -> typename moment<T>::type {
		auto it = moment_cache.find(author);
		if(  it != moment_cache.end() )
		{
			auto& authorEvidences = it->second;
			auto& moment = authorEvidences[ev.index];
			return moment;
		}
		else
		{
			throw std::runtime_error("Cache: Asked for author that wasn't in database: " + author);
		}
	}

	//Determines if a moment returned by "calc_moment_name" is indeterminate (for conditional evidences)
	auto indeterminate(const typename moment<T>::type& m) -> bool {
		return static_cast<T*>(this)->indeterminate(m);
	}

	auto probability(const typename moment<T>::type& m, const double stat) -> double {
		return static_cast<T*>(this)->probability(m,stat);
	}

	auto getRange(const typename moment<T>::type& m) -> std::tuple<double,double> {
		return static_cast<T*>(this)->getRange(m);
	}

protected:
	EvidenceDB& database;
	std::unordered_map<std::string,std::array<typename moment<T>::type,e::func_array_size>> moment_cache;

private:
	std::unordered_map<std::string, double> priori_cache;
};

struct ModifiedGaussian : public BayesianDecisionEngine<ModifiedGaussian>
{
	ModifiedGaussian(EvidenceDB& db);
	auto indeterminate(const moment<ModifiedGaussian>::type& m) -> bool;
	auto probability(const typename moment<ModifiedGaussian>::type& m, const double stat) -> double;
	auto getRange(const typename moment<ModifiedGaussian>::type& m) -> std::tuple<double,double>;

};

struct InterpolatedHistogram : public BayesianDecisionEngine<InterpolatedHistogram>
{
	InterpolatedHistogram(EvidenceDB& db);
	auto indeterminate(const moment<InterpolatedHistogram>::type& m) -> bool;
	auto probability(const typename moment<InterpolatedHistogram>::type& m, const double stat) -> double;
	auto getRange(const typename moment<InterpolatedHistogram>::type& m) -> std::tuple<double,double>;

};

struct Decision {
	using names_type = EvidenceDB::names_type;

	Decision(EvidenceDB& _db);
	auto virtual adviseAllAuthors(const std::string& message) -> std::vector<std::tuple<std::string,double>> = 0;
	auto virtual flushCache() -> void = 0;

	auto advise(const std::string& author, const std::string& message) -> double;
	auto adviseAuthor(const std::string& message) -> std::tuple<std::string,double>;
	auto getAuthorList() -> names_type;

	virtual ~Decision();

protected:
	EvidenceDB& db;
};

struct BayesianDecision : public Decision
{
	using decision_engine = ModifiedGaussian;

	BayesianDecision(EvidenceDB& _db);
	auto adviseAllAuthors(const std::string& message) -> std::vector<std::tuple<std::string,double>>;
	auto flushCache() -> void;

	//std::unique_ptr<ModifiedGaussian> engine;
	std::unique_ptr<decision_engine> engine;
	std::array<double, e::func_array_size> lastStats;

private:
	//Probability constants
	static constexpr double pi2_inv = 1.0/std::sqrt(8*atan(1));

};

struct kNNDecision : public Decision
{
	kNNDecision(EvidenceDB& _db);
	auto virtual adviseAllAuthors(const std::string& message) -> std::vector<std::tuple<std::string,double>>;
	auto virtual flushCache() -> void;

private:
	std::array<std::tuple<double,double>,e::func_array_size> normalize;

};


#endif // DECISION_H
