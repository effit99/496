#include "decision.h"
#include "evidence.h"
#include <cmath>
#include <limits>
#include <sstream>
#include <string>
#include <tuple>
#include <iostream>
#include <algorithm>

Decision::Decision(EvidenceDB &_db) : db{_db} {}

auto Decision::getAuthorList() -> Decision::names_type
{
		return db.getAuthorList();
}

BayesianDecision::BayesianDecision(EvidenceDB& _db)
	: Decision{_db}, engine{new decision_engine(_db)}
{	
	lastStats.fill(0);
}

Decision::~Decision() {}

auto Decision::advise(const std::string& author, const std::string& message) -> double
{

	auto authorProbs = adviseAllAuthors(message);

	for (auto& authorP : authorProbs) {
		if (std::get<0>(authorP) == author) {
			return std::get<1>(authorP);
		}
	}

	throw std::runtime_error("Error: Author not found");

}

auto Decision::adviseAuthor(const std::string& message) -> std::tuple<std::string,double> {

	auto authorProbs = adviseAllAuthors(message);

	return authorProbs.front();
}

struct ProbComparator {
	bool operator()(std::tuple<std::string,double> x1, std::tuple<std::string,double> x2) {
		double p1 = std::get<1>(x1);
		double p2 = std::get<1>(x2);

		return p1 > p2;
	}
};

auto BayesianDecision::adviseAllAuthors(const std::string& message) -> std::vector<std::tuple<std::string,double>> {
	std::vector<std::tuple<std::string,double>> authorProbs;
	double sumProb = 0;
	for (std::size_t i = 0; i < e::func_array_size; i++)
	{
		auto& ev = e::functions[i];
		lastStats[i] = ev.func(message);
	}
	auto names = getAuthorList();
	for (auto& authorRow : names)
	{
		double authorRowProb = engine->calc_priori(authorRow);
		//double authorRowProb = 1; //Testing to see if by not having a priori, we can get better (seems to help?)
		for (std::size_t i = 0; i < e::func_array_size; i++)
		{
			auto& ev = e::functions[i];

			auto moment = engine->calc_moment_name(authorRow,ev);
			if (engine->indeterminate(moment)) {
				continue;
			}

			double stat = lastStats[i];

			//Evidence not present in message; inconclusive; probability 1
			if (ev.conditional && stat == -1) continue;


			double prob = engine->probability(moment, stat);

			authorRowProb *= prob;
		}
		authorProbs.emplace_back(authorRow,authorRowProb);
		sumProb += authorRowProb;
	}

	//Normalize
	for (auto& authorP : authorProbs) {
		std::get<1>(authorP) /= sumProb;
	}

	std::sort(authorProbs.begin(),authorProbs.end(),ProbComparator());

	return authorProbs;

}

auto BayesianDecision::flushCache() -> void
{
	engine = std::unique_ptr<decision_engine>(new decision_engine(db));
}


ModifiedGaussian::ModifiedGaussian(EvidenceDB &db) : BayesianDecisionEngine{db}
{

	//Initialize cache to all -1
	//Any evidences which are indeterminate in training set will therefore be marked as -1
	//Otherwise will be filled in

	for (auto& author : db.getAuthorList()) {
		for (auto& ev : e::functions) {
			moment_cache[author][ev.index] = std::make_tuple(-1,-1);
		}
	}

	//Populate the cache
	for (auto& ev : e::functions)
	{
		auto list = database.calc_gaussian(ev);

		for (auto& authorRow : list){
			std::string name;
			double mean;
			double variance;
			std::tie(name,mean,variance) = authorRow;

			moment_cache[name][ev.index] = std::make_tuple(mean,variance);
		}

	}

}

auto ModifiedGaussian::indeterminate(const moment<ModifiedGaussian>::type& m) -> bool {
	return std::get<0>(m) == -1;
}

//Not real probability.  Gaussian probability density.  Properties desirable.
double ModifiedGaussian::probability(const moment<ModifiedGaussian>::type& m, const double stat)
{
	//Gaussian distribution

	double mean;
	double variance;

	std::tie(mean,variance) = m;

	//Minimum variance
	if (variance < 0.1) variance = 0.1;

	//TODO: Normalize result between (0,1]. Density functions are not necessarily capped at 1.
	double exponent = std::exp(-pow(stat - mean,2)/(2*variance));
	double inv_stdev = 1.0/std::sqrt(variance);

	double probability = /*pi2_inv*inv_stdev**/exponent;

	//TODO:
	//Avoid zero probability.  This is not correct.
	if (probability == 0) probability = std::numeric_limits<double>::min();
	//if (probability == 0) probability = 0.1; //Not correct

	return probability;

}

auto ModifiedGaussian::getRange(const typename moment<ModifiedGaussian>::type& m) -> std::tuple<double,double> {
	double mean;
	double variance;
	std::tie(mean,variance) = m;
	double start = mean;
	while(probability(m,start) > 0.0001)
	{
		start -= 0.1;
	}

	double end = 2*mean - start;

	return std::make_tuple(start,end);
}

InterpolatedHistogram::InterpolatedHistogram(EvidenceDB& db) : BayesianDecisionEngine{db} {

	auto authors = db.getAuthorList();

	for (auto& author : authors) {
		for (auto& ev : e::functions) {
			auto vals = db.get_evidence_values(author,ev);
			moment<InterpolatedHistogram>::type hist;

			//Determine range of values for constructing the histogram

			//Sort values for easy access
			std::sort(vals.begin(), vals.end());

			double min = vals.front();
			double max = vals.back();

			double bucketSize = 1;
			if (vals.size() > 1) {
				auto first = vals.cbegin();
				auto second = vals.cbegin();
				second++;
				double optimalDistance = 0;
				while (second != vals.cend()) {
					if (*second - *first > optimalDistance)
						optimalDistance = *second - *first;

					first++;
					second++;
				}
				if (optimalDistance != 0) {
					bucketSize = optimalDistance;
				}
			}


			//Construct n histogram bars (including one at either end)
			double start = min - bucketSize;
			double end = max + bucketSize;
			for (double i = start; i <= end; i += bucketSize) {
				hist[i] = 0;
			}

			for (auto val : vals) {
				if (val == -1) {
					continue; //Do not count indeterminate results
				}
				//Each histogram "bar" has a tolerance of +-range/2
				bool found = false;
				for (auto& bucket : hist) {
					if (val < bucket.first - bucketSize/2) {
						break;
					} else if (bucket.first - bucketSize/2 <= val && val <= bucket.first + bucketSize/2){
						//Found bucket, increment
						bucket.second++;
						found = true;
						break;
					}
				}
				if (!found) {
					std::cout << "Bug detected" << std::endl;
				}
			}



			/*
			//Empty histograms denote indeterminate training set for evidence
			for (auto val : vals) {
				if (val == -1) {
					continue; //Do not count indeterminate results
				}
				//Tolerance is +- 0.5.  If in that range, increment existing bucket.  Otherwise, add new bucket.
				bool shouldAdd = true;
				for (auto& bucket : hist) {
					if (val > bucket.first - 0.5 && val < bucket.first + 0.5 ) {
						bucket.second++;
						shouldAdd = false;
						break;
					}
					if (val <= bucket.first - 0.5) { //List is sorted by keys in ascending order, so no need to check once val is less than the threshold
						break;
					}
				}
				if (shouldAdd) {
					hist[val] = 1;
				}
			}*/

			moment_cache[author][ev.index] = hist;
		}
	}

}

auto InterpolatedHistogram::indeterminate(const moment<InterpolatedHistogram>::type& m) -> bool {
	//return m.empty(); //Empty histograms denote indeterminant traning set evidences
	double max = 0;
	for (auto& bucket : m) {
		if (bucket.first > max) max = bucket.first;
	}

	return max == 0;

}

auto InterpolatedHistogram::probability(const typename moment<InterpolatedHistogram>::type& m, const double stat) -> double {

	//Find where "stat" lies in the histogram and interpolate based on the buckets on either side
	//Interpolate to 0 if "outside" of histogram--1 unit away from outside buckets

	//Define a smallest probability to avoid the problem of having a zero probability cancelling everything out
	constexpr double smallestProb = 0.01;

	//First, find highest frequency
	double highest = 0;
	for (auto& bucket : m) {
		if (bucket.second > highest) highest = bucket.second;
	}

	double start;
	double end;
	std::tie(start,end) = getRange(m);

	//If outside of histogram, interpolate to smallestProb
	if (stat <= start) {
		//Interpolate range [start - 1, start]
		/*double diff = stat - (start - 1);
		if (diff < 0) diff = 0;
		double result = smallestProb + ((m.at(start))/highest - smallestProb)*diff;
		if (result <= smallestProb) result = smallestProb;
		return result;*/
		return smallestProb;
	}

	if (stat >= end) {
		//Interpolate range [end, end + 1]
		/*double diff = (end + 1) - stat;
		if (diff < 0) diff = 0;
		double result = smallestProb + ((m.at(end))/highest - smallestProb)*diff;
		if (result <= smallestProb) result = smallestProb;
		return result;*/
		return smallestProb;
	}

	//Not outside histogram, so find which buckets stat is between and interpolate
	//At this point we are guaranteed at least two buckets in the histogram
	auto left = m.cbegin();
	auto right = m.cbegin();
	right++;
	while (right != m.cend()) {
		if (left->first <= stat && stat <= right->first) {
			//Found the two buckets, perform interpolation
			double xdiff = right->first - left->first;
			double ydiff = right->second - left->second;
			double delta = stat - left->first;

			double result = (left->second + (ydiff*delta/xdiff))/highest;
			if (result <= smallestProb) result = smallestProb;
			return result;
		}

		left++;
		right++;
	}

	//This should never be executed
	std::cout << "Executed anyway..." << std::endl;
	return 0.01;

}

auto InterpolatedHistogram::getRange(const typename moment<InterpolatedHistogram>::type& m) -> std::tuple<double,double> {

	auto start = m.cbegin()->first;
	auto end = m.crbegin()->first;

	return std::make_tuple(start,end);
}


kNNDecision::kNNDecision(EvidenceDB& _db) : Decision{_db}{

	//Get normalization constants for each evidence

	for (auto& ev : e::functions){
		std::stringstream s;
		std::stringstream indet;
		s << "SELECT min(" << ev.name << "), " << "max(" << ev.name << ")" << " FROM evidence WHERE " << ev.name << " != -1";
		indet << "SELECT avg(" << ev.name << ")" << " FROM evidence";

		auto check = db.statement(indet.str());
		check.step();

		double min;
		double max;

		if (check.column<double>(0) == -1) {
			min = -1;
			max = -1;
		} else {
			auto stmt = db.statement(s.str());

			stmt.step();
			min = stmt.column<double>(0);
			max = stmt.column<double>(1);
		}

		normalize[ev.index] = std::make_tuple(min,max);

	}

}

struct AuthorDistComparator {
	bool operator()(std::tuple<std::string,double> x1, std::tuple<std::string,double> x2) {
		double d1 = std::get<1>(x1);
		double d2 = std::get<1>(x2);

		return d1 < d2;
	}
};

std::vector<std::tuple<std::string,double>> kNNDecision::adviseAllAuthors(const std::string& message) {

	std::array<double,e::func_array_size> stats;

	for (std::size_t i = 0; i < e::func_array_size; i++)
	{
		auto& ev = e::functions[i];
		stats[i] = ev.func(message);
	}

	std::vector<std::tuple<std::string,double>> authorRanking;

	auto names = getAuthorList();

	for (auto& name : names) {
		auto evStmt = db.statement("SELECT * FROM evidence WHERE name = ?");

		evStmt.bind(name,1);

		double minNormSquared = std::numeric_limits<double>::max();
		while (evStmt.step() == SQLITE_ROW) {
			//Evidences begin at column index 3
			//Compute euclidean distance
			double normSquared = 0; //Will be eva^2 + evb^2 + ...
			//std::cout << "Begin processing" << std::endl;
			for (int i = 0; i < e::func_array_size; i++) {
				double min;
				double max;
				std::tie(min,max) = normalize[i];
				if (min == -1 || max == -1) {
					continue; //Indeterminate
				} else if (stats[i] == -1 && e::functions[i].conditional) {
					continue; //Indeterminate
				} else {
					//Normalize: evidence min becomes 0, evidence max becomes 1
					double diff;
					if (max == min) {
						//Cannot normalize -- fall back on default
						diff = stats[i] - evStmt.column<double>(i+3);
					} else {
						double interpStat = (stats[i] - min)/(max - min);
						double interpPoint = (evStmt.column<double>(i+3) - min)/(max - min);
						//std::cout << "interpStat: " << interpStat << " interpPoint: " << interpPoint << " max: " << max << " min: " << min << " stat: " << stats[i] << std::endl;

						diff = interpStat - interpPoint;
					}
					normSquared += diff*diff;

				}
			}
			//std::cout << "calculated normSquared: " << normSquared << std::endl;

			if (normSquared < minNormSquared)
				minNormSquared = normSquared;
		}

		authorRanking.emplace_back(std::make_tuple(name,minNormSquared));
	}

	std::sort(authorRanking.begin(), authorRanking.end(), AuthorDistComparator());

	//Provide readable result: Interpolate between closest being 100% prob and furthest being 0% prob
	double max = std::get<1>(authorRanking.back());
	double min = std::get<1>(authorRanking.front());

	for (auto& authorRank : authorRanking) {
		auto& dist = std::get<1>(authorRank);
		double prob = (max - dist)/(max - min);
		dist = prob;
	}

	return authorRanking;

}
auto kNNDecision::flushCache() -> void {
	//No cache, so no need
}
