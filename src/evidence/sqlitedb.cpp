#include "evidence.h"
#include "sqlitedb.h"
#include "sqlite3.h"
#include <array>
#include <sstream>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

void SQLiteStmt::bind(int t, int i)
{
	if (int retval = sqlite3_bind_int(p_stmt, i, t) != SQLITE_OK)
	{
		throw SQLiteException(database.dbHandle,retval);
	}
}

auto SQLiteStmt::bind(double t, int i) -> void
{
	if (int retval = sqlite3_bind_double(p_stmt, i, t) != SQLITE_OK)
	{
		throw SQLiteException(database.dbHandle,retval);
	}
}

void SQLiteStmt::bind(const std::string& text, int i)
{
	if (int retval = sqlite3_bind_text(p_stmt, i, text.c_str(), text.length(), SQLITE_TRANSIENT) != SQLITE_OK)
	{
		throw SQLiteException(database.dbHandle, retval);
	}
}

int SQLiteStmt::step()
{
	int r = sqlite3_step(p_stmt);
	if (r == SQLITE_ROW || r == SQLITE_DONE)
	{
		return r;
	}
	else
	{
		throw SQLiteException(database.dbHandle, r);
	}
}

void SQLiteStmt::reset()
{
	if (int retval = sqlite3_reset(p_stmt) != SQLITE_OK)
	{
		throw SQLiteException(database.dbHandle, retval);
	}
}

int SQLiteStmt::columns()
{
	return sqlite3_column_count(p_stmt);
}

SQLiteStmt::~SQLiteStmt()
{
	if (int retval = sqlite3_finalize(p_stmt) != SQLITE_OK)
	{
		throw SQLiteException(database.dbHandle, retval);
	}
}

SQLiteStmt::SQLiteStmt(SQLiteDB& db, const std::string& stmt) : database(db)
{
	if (int retval = sqlite3_prepare_v2(db.dbHandle, stmt.c_str(), stmt.length(), &p_stmt,nullptr)
			!= SQLITE_OK)
	{
		throw SQLiteException(database.dbHandle, retval);
	}
}

SQLiteStmt::SQLiteStmt(SQLiteStmt&& tmp) : database(tmp.database)
{
	p_stmt = tmp.p_stmt;
	tmp.p_stmt = nullptr;
}


template<> auto SQLiteStmt::column(int col) -> int
{
	return sqlite3_column_int(p_stmt, col);
}


template<> auto SQLiteStmt::column(int col) -> std::string
{
	return std::string(reinterpret_cast<const char *>(sqlite3_column_text(p_stmt, col)));
}

template<> auto SQLiteStmt::column(int col) -> double
{
	return sqlite3_column_double(p_stmt, col);
}


SQLiteDB::SQLiteDB(const std::string& filename)
{
	fileName = filename;
	if (int retval = sqlite3_open_v2(fileName.c_str(), &dbHandle, SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE, nullptr) != SQLITE_OK)
	{
		throw SQLiteException(dbHandle, retval);
	}

	sqlite3_enable_load_extension(dbHandle,1);
	char *err;
#if defined(__linux) || defined(__APPLE__)
    if (sqlite3_load_extension(dbHandle,"../../sqlite/libsqlitefunctions.so",nullptr,&err) == SQLITE_ERROR)
#else
    if (sqlite3_load_extension(dbHandle,"../../sqlite/libsqlitefunctions",nullptr,&err) == SQLITE_ERROR)
#endif
    {
		throw std::runtime_error(err);
	}

}

SQLiteDB::SQLiteDB() : SQLiteDB{":memory:"}
{

}

SQLiteDB::SQLiteDB(SQLiteDB&& tmp) : fileName{tmp.fileName}, dbHandle{tmp.dbHandle}
{
	tmp.dbHandle = nullptr;
}

auto SQLiteDB::statement(const std::string& stmt) -> SQLiteStmt
{
	return {*this,stmt};
}

SQLiteDB::~SQLiteDB()
{
	if (int retval = sqlite3_close_v2(dbHandle) != SQLITE_OK)
	{
		//throw std::runtime_error("SQLiteDB: Could not close " + fileName);
		throw SQLiteException(dbHandle, retval);
	}

}


SQLiteException::SQLiteException(sqlite3* db, int retval)
{
	if (retval == SQLITE_ERROR)
	{
		errcode = sqlite3_errcode(db);
		errstr = sqlite3_errstr(errcode);
		errmsg = sqlite3_errmsg(db);
	} else if (retval == SQLITE_MISUSE)
	{
		errcode = SQLITE_MISUSE;
		errstr = "MISUSE";
		errmsg = "Function misused!";
	} else {
		errcode = -1;
		errstr = "UNKNOWN";
		errmsg = "I have no idea what you did, and neither does SQLite";
	}
}

auto SQLiteException::what() const noexcept -> const char *
{
	std::stringstream s;

	s << errstr << ": " <<  errmsg << " (return code: " << errcode << ")";

	return s.str().c_str();

}


void EvidenceDB::initDB() {
	//Create table with evidences
	std::stringstream s;
	s << "CREATE TABLE evidence ( id INTEGER NOT NULL, name NOT NULL, message NOT NULL, ";
	for (auto ev : e::functions)
	{
		s << ev.name << " REAL NOT NULL, ";
	}
	s << "CONSTRAINT id_pk PRIMARY KEY (id) );";

	statement(s.str()).step();
}

EvidenceDB::EvidenceDB()
{
	initDB();
}

EvidenceDB::EvidenceDB(EvidenceDB&& db) : SQLiteDB{std::forward<SQLiteDB>(db)} { std::cout << "moved" << std::endl; }

EvidenceDB::EvidenceDB(EvidenceDB& tmp)
{

	//std::cout << "copying db" << std::endl;
	initDB();

	auto fetchStmt = tmp.statement("SELECT * FROM evidence");

	std::stringstream s;

	using size_type = e::func_array_type::size_type;
	size_type max_size = std::tuple_size<e::func_array_type>::value;

	s << "INSERT INTO evidence VALUES (NULL, ?, ?, ";

	for (size_type i = 0; i < max_size - 1; i++)
	{
		s << "?, ";
	}
	s << "? )";


	while (fetchStmt.step() == SQLITE_ROW) {
		auto insertStmt = statement(s.str());

		insertStmt.bind(fetchStmt.column<std::string>(1),1);
		insertStmt.bind(fetchStmt.column<std::string>(2),2);

		for (size_type i = 0; i < max_size; i++) {
			insertStmt.bind(fetchStmt.column<double>(3+i),3+i);
		}
		insertStmt.step();
	}

	std::cout << "copied" << std::endl;

}

auto EvidenceDB::getAuthorList() -> names_type
{

	std::string str_stmt = "SELECT DISTINCT name FROM evidence LIMIT 2000";
	SQLiteStmt stmt = statement(str_stmt);
	std::vector<std::string> names{};
	while (stmt.step() == SQLITE_ROW)
	{
		names.push_back(stmt.column<std::string>(0));
	}

	return std::move(names);
}

auto EvidenceDB::insertMessage(const std::string& name, const std::string& message) -> void
{
	std::stringstream s;

	using size_type = e::func_array_type::size_type;
	size_type max_size = std::tuple_size<e::func_array_type>::value;

	s << "INSERT INTO evidence VALUES (NULL, ?, ?, ";


	for (size_type i = 0; i < max_size - 1; i++)
	{
		s << "?, ";
	}
	s << "? )";

	std::string str_stmt = s.str();

	SQLiteStmt stmt = statement(str_stmt);

	stmt.bind(name, 1);
	stmt.bind(message, 2);

	for (size_type i = 0; i < max_size; i++)
	{
		stmt.bind(e::functions[i].func(message), i+3);
	}

	stmt.step();

}

/*
auto EvidenceDB::calc_ni(const std::string& name) -> int
{
	using size_type = e::func_array_type::size_type;

	constexpr size_type max = std::tuple_size<e::func_array_type>::value;

	std::stringstream query;
	query << "SELECT sum( ";

	size_type i;
	for (i = 0; i < max-1; i++)
	{
		query << e::functions[i].name << "+";
	}
	query << e::functions[i].name << ") FROM evidence WHERE name == ?";

	SQLiteStmt stmt = statement(query.str());
	stmt.bind(name, 1);
	stmt.step();
	return stmt.column<int>(0);
}*/

auto EvidenceDB::calc_priori(const std::string& name) -> double
{
	SQLiteStmt stmt = statement("SELECT count(evidence.name)"
							   "/cast(e2.total AS REAL) AS PCi FROM evidence CROSS JOIN (SELECT "
							   "count(evidence.name) AS total FROM evidence) "
							   "AS e2 WHERE evidence.name == ? GROUP BY evidence.name");

	stmt.bind(name, 1);
    stmt.step();

	return stmt.column<double>(0);
}

auto EvidenceDB::calc_gaussian(const e::Evidence& ev)
-> const std::vector<EvidenceDB::gauss_type>
{
	std::vector<std::tuple<const std::string, const double, const double>> results;

	std::stringstream s;

	//Cannot bind text for select column in SQLite
	s << "SELECT name, avg(" << ev.name << "), variance(" << ev.name << ") FROM evidence WHERE "
		 << ev.name << " != -1 GROUP BY name";
	SQLiteStmt stmt = statement(s.str());

	while (stmt.step() == SQLITE_ROW)
	{
		std::string name = stmt.column<std::string>(0);
		double avg = stmt.column<double>(1);
		double var = stmt.column<double>(2);
		results.push_back(std::make_tuple(name,avg,var));
	}

	return std::move(results);
}

auto EvidenceDB::get_evidence_values(const std::string& name, const e::Evidence& ev) -> std::vector<double> {

	std::stringstream s;
	std::vector<double> resultSet;

	s << "SELECT " << ev.name << " FROM evidence WHERE name = ?";

	auto stmt = statement(s.str());
	stmt.bind(name,1);
	while (stmt.step() == SQLITE_ROW) {
		resultSet.emplace_back(stmt.column<double>(0));
	}

	return resultSet;

}

/*
auto EvidenceDB::calc_meanvariance_name(const std::string& name, const e::Evidence& ev)
-> meanvariance_type
{
    std::stringstream s;

	//Cannot bind text for select column in SQLite
	s << "SELECT avg(" << ev.name << "), variance(" << ev.name << ") FROM evidence WHERE name = ? and "
		 << ev.name << " != -1";
	SQLiteStmt stmt = statement(s.str());

	stmt.bind(name, 1);

	stmt.step();
	if (stmt.columns() == 0) //No rows selected; all -1
	{
		return std::make_tuple(-1,0);
	}


	double mean = stmt.column<double>(0);
    double variance = stmt.column<double>(1);

	return std::make_tuple(mean, variance);

}*/
