#include "evidence.h"
#include "parser.h"
#include "sqlite3.h"
#include "sqlitedb.h"
#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

Parser::Parser(const std::string& fileName) : file_name{fileName} {}

auto Parser::getFilename() -> std::string
{
	return file_name;
}

LogParser::LogParser(const std::string& fileName) : Parser{fileName}, file{fileName}
{

}

auto LogParser::parseFile() -> std::unique_ptr<EvidenceDB>
{

	std::ifstream file(file_name);

	std::string line;

	std::unique_ptr<EvidenceDB> db{new EvidenceDB{}};

	while (std::getline(file,line))
	{
		auto msg_pos = line.find_first_of(">)");
		if (msg_pos != std::string::npos){
			std::string name = line.substr(0, msg_pos+1);
			std::string message = line.substr(msg_pos+2);
			db->insertMessage(name,message);
		} else {
			throw std::runtime_error("LogParser: Could not parse file: "
									 + file_name
									 + ". Line: "
									 + line);
		}

	}

	return std::move(db);
}
auto LogParser::getDescription() -> std::string
{
	return {"Chat Log"};
}

SQLiteParser::SQLiteParser(const std::string& fileName) : Parser{fileName}
{

}

auto SQLiteParser::parseFile() -> std::unique_ptr<EvidenceDB>
{

    SQLiteDB sdb(file_name);
	std::unique_ptr<EvidenceDB> edb{new EvidenceDB{}};

	SQLiteStmt begin_stmt = edb->statement("BEGIN;");
	SQLiteStmt end_stmt = edb->statement("END;");
	SQLiteStmt get_stmt = sdb.statement("SELECT sender, content FROM corpus");

	begin_stmt.step();

    while (get_stmt.step() == SQLITE_ROW)
	{
		//if(count++ % 4000 == 0) For optimizing the transaction's, there should be some optimal value of inserts per transaction
		//                          could be worth expermenting with later if we want to speed up the parsing
		//{
		//    edb->statement("END").step();
		//    edb->statement("BEGIN").step();
		//}
		edb->insertMessage(get_stmt.column<std::string>(0),get_stmt.column<std::string>(1));
		//std::cerr << "sender: " << get_stmt.column<std::string>(0) << " | " << get_stmt.column<std::string>(1) << std::endl;
    }

	end_stmt.step();

	return std::move(edb);
}

auto SQLiteParser::getDescription() -> std::string
{
	return {"SMS Corpus"};
}
