#include "evidence.h"
#include <algorithm>
#include <cctype>
#include <cstring>
#include <fstream>
#include <iostream>
#include <locale>
#include <unordered_set>
#include <vector>

//Anonymous namespace is preferred to static linkage
namespace {

using e_list = std::vector<std::string>;
e_list emoticon_list;
e_list first_person_pronoun_list;
e_list second_person_pronoun_list;
e_list third_person_pronoun_list;
e_list interogative_list;
e_list negative_list;
e_list apologetic_list;
e_list positive_list;
e_list acronym_list;

inline auto count_occurrences(std::string msg, std::string subst) -> double
{
	std::string::size_type pos = 0;
	int count = 0;
	while ((pos = msg.find(subst, pos)) != std::string::npos){
		count++;
		pos++;
	}

	return count;
}

auto init_list(e_list &list, std::string file_path) -> void
{
	std::ifstream file(file_path, std::ios::in);
	std::string in;

	while(file >> in) //Read in the newline delimited file.
	{
		list.push_back(in);
	}

}

auto check_list(const std::string& msg, e_list& list) -> double
{
	int count = 0;
    std::string::size_type index;
    std::string mutable_msg(msg);

	for(auto& element : list)
	{
        while( (index = mutable_msg.find(element)) != std::string::npos )
        {
            ++count;
            mutable_msg.erase(index,element.length());
        }
	}

	return count;
}

//Delimits messages into individual words before checking against a list, as opposed to check_list which is happy with a substring.
auto hunt_for_red_october(const std::string& msg, e_list list) -> double
{
    std::string last_word;
    std::string mutable_msg(msg);
    std::transform(mutable_msg.begin(), mutable_msg.end(), mutable_msg.begin(), ::tolower);

    bool in_word = false;
    int count = 0;

    const std::locale& loc = std::locale::classic();

    for(char c : msg)
    {
        if ( !std::isspace(c, loc))
        {
            if(!in_word)
            {
                for(auto element : list)
                {
                    std::transform(last_word.begin(), last_word.end(), last_word.begin(), ::tolower);
                    if(element == last_word)
                    {
                        count++;
                    }
                }

                last_word = "";
                in_word = true;
            }
            last_word.push_back(c);
        }
        else
        {
            in_word = false;
        }
    }

    for(auto element : list)
    {
        std::transform(last_word.begin(), last_word.end(), last_word.begin(), ::tolower);
        if(element == last_word)
        {
            count++;
        }
    }

    return count;
}
}

namespace e
{

//Not sure if undefined behaviour.  Standard says per file, globals initialized from top to bottom
//in that order.  Want each evidence to have its array index stored within it.  Doubles as a unique
//ID for an evidence.  Standard says nothing about which files get initialized first
std::size_t Evidence::maxIndex = 0;

Evidence::Evidence(double(*fcn)(const std::string&), const std::string& description)
	: func{fcn}, name{description}, index{maxIndex++}, conditional{false} {}

Evidence::Evidence(double(*fcn)(const std::string&), const std::string& description, bool cond)
	: func{fcn}, name{description}, index{maxIndex++}, conditional{cond} {}

/*
IMPORTANT:
When a new evidence is created, add it to this array.  You'll need to modify in the header
file the array type to accomodate the new space.  There needs to be exactly the same number
of entries in the array as the array can hold, otherwise a compile error *will* result.
This means you can't have less evidences in the array than the array could hold at maximum.
If you get compile errors after adding something in here, check the definition of func_array_type
*/
func_array_type functions{//Cast needed due to possible bug in g++?
(Evidence){first_capital_sentence, "first_capital_sentence",true},
{first_capital_message, "first_capital_message",true},
{avg_period_space, "avg_period_space",true},
{emoticons, "emoticons"},
{punctuation, "punctuation"},
{word_count,"word_count"},
{avg_word_length,"avg_word_length",true},
{length_chars,"length_chars"},
//{length_words,"length_words"},
{distinct_word_count,"distinct_words_count"},
{distinct_char_count,"distinct_char_count"},
{first_person_pronoun_count,"first_person_pronoun_count"},
{second_person_pronoun_count,"second_person_pronoun_count"},
{third_person_pronoun_count,"third_person_pronoun_count"},
{interogative_count,"interogative_count"},
{negative_count,"negative_count"},
{positive_count,"positive_count"},
{apologetic_count,"apologetic_count"},
{capital_count,"capital_count"},
{lowercase_count,"lowercase_count"},
{exclamation_count,"exclamation_count"},
{question_mark_count,"question_mark_count"},
{comma_count,"comma_count"},
{space_count,"space_count"},
{vowel_count,"vowel_count"},
{consonant_count,"consonant_count"},
{number_count,"number_count"},
{message_period_end,"message_period_end"},
{im_cond,"im_cond",true},
{i_cond,"i_cond",true},
{alot_cond,"alot_cond",true},
{yeah_cond,"yeah_cond",true}
};

auto init() -> void {
	init_list(emoticon_list, "../../../ev/emoticons.txt");
    init_list(first_person_pronoun_list,"../../../ev/first_person_pronouns.txt");
    init_list(second_person_pronoun_list,"../../../ev/second_person_pronouns.txt");
    init_list(third_person_pronoun_list,"../../../ev/third_person_pronouns.txt");
    init_list(interogative_list,"../../../ev/interogatives.txt");
    init_list(negative_list,"../../../ev/negatives.txt");
    init_list(apologetic_list,"../../../ev/apologetics.txt");
    init_list(positive_list,"../../../ev/positives.txt");
    init_list(acronym_list,"../../../ev/acronyms.txt");

	/*
	for (std::size_t i = 0; i < e::func_array_size; i++)
	{
		e::functions[i].index = i;
	}*/
}


//Number of times the first letter of a sentence is a capital
//Probability of capital letter at start of sentence
//Conditional
auto first_capital_sentence(const std::string& msg) -> double
{
	bool isFirst = false;
    const std::locale& loc = std::locale::classic();
	int capitals = 0;
	int sentences = 0;
	if (msg.length() < 2) return 0; //TODO: What to do here?  return -1?
	for(char c : msg.substr(1)){
		if (c == '.' || c == '!' || c == '?') {
            isFirst = true;
		} else if (std::isupper(c, loc) && isFirst){
			++capitals;
			++sentences;
            isFirst = false;
		} else if (std::islower(c, loc) && isFirst){
			++sentences;
			isFirst = false;
		} else if (!std::isspace(c, loc) && isFirst){
			isFirst = false;
		}
    }

	if (sentences == 0) return -1;

	return (double)capitals/(double)sentences;
}

//First word in message is capitalized
//Conditional
auto first_capital_message(const std::string& msg) -> double
{
	if (std::isalpha(msg.front(),std::locale::classic())){
		return std::isupper(msg.front(),std::locale::classic());
	}
    return -1;
}

//Number of occurrences of single space after period
auto period_space(const std::string& msg) -> double
{
	return count_occurrences(msg, ". ");
}
//Returns number of occurrences of double spaces after a period in a message.
auto period_double_space(const std::string& msg) -> double
{
	return count_occurrences(msg, ".  ");
}

//Average number of spaces that follow periods
//Conditional
auto avg_period_space(const std::string& msg) -> double
{
	const std::locale& loc = std::locale::classic();
	bool found = false; //last non-space
	int total_periods=0;
	int space_count=0;
	//std::vector<int> count_list;

	for(char c : msg.substr(0,msg.size()-1))
	{
		if(c == '.' || c == '!' || c == '?')
		{
			++total_periods;
			found=true;
		}
		else if(found && std::isspace(c,loc))
			++space_count;
		else
		{
			found = false;
		}
	}
	return (total_periods == 0.0) ? -1 : ((double)space_count)/((double)total_periods);
}

//Get the number of emoticons used in the message
auto emoticons(const std::string& msg) -> double
{
    return check_list(msg,emoticon_list);
}

//Get the number of bits of punctuation used in the message
auto punctuation(const std::string& msg) -> double
{
	int count = 0;
	for (char c : msg) {
		if (std::ispunct(c, std::locale::classic())) count++;
	}

	return count;
}

//Number of words in a message - word defined to be anything but whitespace or punctuation.
auto word_count(const std::string& msg) -> double
{
    int words = 0;
    bool in_word = false;
    const std::locale& loc = std::locale::classic();

    for(char c : msg)
    {
        if ( !std::ispunct(c, loc) && !std::isspace(c, loc))
        {
            words += (!in_word) ? 1 : 0;
            in_word = true;
        }
        else
        {
            in_word = false;
        }
    }

    return words;
}

//Average length of the words in the message - truncated, no decimals.
//Conditional
auto avg_word_length(const std::string& msg) -> double
{
    int words = 0;
    int total_length = 0;
    bool in_word = false;

    const std::locale& loc = std::locale::classic();

    for(char c : msg)
    {
        if ( !std::ispunct(c, loc) && !std::isspace(c, loc))
        {
            words += (!in_word) ? 1 : 0;
            in_word = true;
            total_length++;
        }
        else
        {
            in_word = false;
        }
    }

    return (words != 0) ? (int)(total_length / words) : -1;
}

//Length of the message by non-white space character.
auto length_chars(const std::string& msg) -> double
{
    int total_length = 0;

    const std::locale& loc = std::locale::classic();

    for(char c : msg)
    {
        if ( !std::isspace(c, loc))
        {
               total_length++;
        }
    }

    return total_length;
}

//Length of message by word count - numbers count as words.
auto length_words(const std::string& msg) -> double
{
    int words = 0;
    bool in_word = false;

    const std::locale& loc = std::locale::classic();

    for(char c : msg)
    {
        if ( !std::ispunct(c, loc) && !std::isspace(c, loc))
        {
            words += (!in_word) ? 1 : 0;
            in_word = true;
        }
        else
        {
            in_word = false;
        }
    }

    return words;
}

//Number of distinct words in a given message. Numbers are considered words.
auto distinct_word_count(const std::string& msg) -> double
{
    std::unordered_set<std::string> seen_words;
    std::string last_word;

    bool in_word = false;

    const std::locale& loc = std::locale::classic();

    for(char c : msg)
    {
        if ( !std::ispunct(c, loc) && !std::isspace(c, loc))
        {
            if(!in_word)
            {
                seen_words.insert(last_word);
                last_word = "";
                in_word = true;
            }
            last_word.push_back(c);
        }
        else
        {
            in_word = false;
        }
    }

    seen_words.insert(last_word);

    return seen_words.size() - 1;
}

//Number of distinct characters in a message - everything is considered a character except whitespace.
auto distinct_char_count(const std::string& msg) -> double
{
    std::unordered_set<char> seen_chars;

    const std::locale& loc = std::locale::classic();

    for(char c : msg)
    {
        if ( !std::isspace(c, loc) )
        {
            seen_chars.insert(c);
        }
    }

    return seen_chars.size() - 1;
}

//Counts the number of first person pronouns like 'i' and 'me', case insensitive.
auto first_person_pronoun_count(const std::string& msg) -> double
{
    return hunt_for_red_october(msg,first_person_pronoun_list);
}

//Counts second person pronouns and such things
auto second_person_pronoun_count(const std::string& msg) -> double
{
    return hunt_for_red_october(msg,second_person_pronoun_list);
}

//Third person, you get the drill
auto third_person_pronoun_count(const std::string& msg) -> double
{
    return hunt_for_red_october(msg,third_person_pronoun_list);
}

auto interogative_count(const std::string& msg) -> double
{
    return hunt_for_red_october(msg,interogative_list);
}

auto negative_count(const std::string& msg) -> double
{
    return hunt_for_red_october(msg,negative_list);
}

auto positive_count(const std::string& msg) -> double
{
    return hunt_for_red_october(msg, positive_list);
}
auto apologetic_count(const std::string& msg) -> double
{
    return hunt_for_red_october(msg,apologetic_list);
}

auto capital_count(const std::string& msg) -> double
{
    int count = 0;

    for(auto c : msg)
    {
        if( std::isupper(c) )
            count++;
    }

    return count;
}

auto lowercase_count(const std::string& msg) -> double
{
    int count = 0;

    for(auto c : msg)
    {
        if( std::islower(c) )
            count++;
    }

    return count;
}
auto exclamation_count(const std::string& msg) -> double
{
    int count = 0;

    for(auto c : msg)
    {
        if( c == '!' )
            count++;
    }

    return count;
}

auto question_mark_count(const std::string& msg) -> double
{
    int count = 0;

    for(auto c : msg)
    {
        if( c == '?' )
            count++;
    }

    return count;
}
auto comma_count(const std::string& msg) -> double
{
    int count = 0;

    for(auto c : msg)
    {
        if( c == ',' )
            count++;
    }

    return count;
}

auto space_count(const std::string& msg) -> double
{
    int count = 0;

    for(auto c : msg)
    {
        if( std::isspace(c) )
            count++;
    }

    return count;
}

auto vowel_count(const std::string& msg) -> double
{
    int count = 0;
    std::unordered_set<char> vowels = { 'a', 'e', 'i', 'o', 'u', 'y' };

    for(auto c : msg)
    {
        if( vowels.count(std::tolower(c)) != 0)
            count++;
    }

    return count;
}

auto consonant_count(const std::string& msg) -> double
{
    int count = 0;
    std::unordered_set<char> consonants = { 'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z' };

    for(auto c : msg)
    {
        if( consonants.count(std::tolower(c)) != 0)
            count++;
    }

    return count;

}

auto number_count(const std::string& msg) -> double
{
    int count = 0;

    for(auto c: msg)
    {
        if( std::isdigit(c) )
            count++;

    }

    return count++;
}


auto set_evidence(const Evidence& e, const bool enabled) -> void
{
    for( func_array_type::iterator it = functions.begin(); it != functions.end(); it++)
    {
        it->enabled = (it->name == e.name) ? enabled : it->enabled;
	}
}

auto start_sentence(const std::string& msg, std::string::size_type i) -> bool
{
	while (i != 0)
	{
		--i;
		auto c = msg[i];
		if (std::isalpha(c,std::locale::classic()))
			return false;
		if (c == '?' || c == '!' || c == '.')
			return true;
	}

	return true;
}

auto message_period_end(const std::string& msg) -> double
{
	auto c = msg.back();
	return (c == '.' || c == '?' || c == '!');
}

//Returns -1 for no data, 1 for im, 2 for i'm, 3 for I'm
auto im_cond(const std::string& msg) -> double
{
	int no_apos_count = 0;
	int apos_count = 0;
	int capital_count = 0;

	using size_type = std::string::size_type;

	size_type result = 0;
	while ((result = msg.find("im", result)) != std::string::npos)
	{
		if (!start_sentence(msg, result))
			++no_apos_count;
		++result;
	}

	result = 0;
	while ((result = msg.find("i'm", result)) != std::string::npos)
	{
		if (!start_sentence(msg, result))
			++apos_count;
		++result;
	}

	result = 0;
	while ((result = msg.find("I'm", result)) != std::string::npos)
	{
		if (!start_sentence(msg, result))
			++capital_count;
		++result;
	}

	//Inconclusive: No occurrence of im, i'm, or I'm in message
	if (no_apos_count == 0 && apos_count == 0 && capital_count == 0)
		return -1;

	double denominator = no_apos_count + apos_count + capital_count;
	double numerator = no_apos_count + apos_count*2 + capital_count*3;

	return numerator/denominator;
}

//returns -1 for no data, 1 for i, 2 for I
auto i_cond(const std::string& msg) -> double
{
	int i_count = 0;
	int I_count = 0;

	using size_type = std::string::size_type;

	size_type result = 0;
	while ((result = msg.find(" i ", result)) != std::string::npos)
	{
		if (!start_sentence(msg, result))
			++i_count;
		++result;
	}

	result = 0;
	while ((result = msg.find(" I ", result)) != std::string::npos)
	{
		if (!start_sentence(msg, result))
			++I_count;
		++result;
	}

	//Inconclusive:
	if (i_count == 0 && I_count == 0)
		return -1;

	double denominator = i_count + I_count;
	double numerator = i_count + I_count*2;

	return numerator/denominator;
}

auto alot_cond(const std::string& msg) -> double
{
	int alot_count = 0;
	int a_lot_count = 0;

	using size_type = std::string::size_type;

	std::string mymsg = msg;

	std::transform(mymsg.begin(), mymsg.end(), mymsg.begin(), ::tolower);

	size_type result = 0;
	while ((result = mymsg.find("alot", result)) != std::string::npos)
	{
		++alot_count;
		++result;
	}

	result = 0;
	while ((result = mymsg.find("a lot", result)) != std::string::npos)
	{
		++a_lot_count;
		++result;
	}

	//Inconclusive:
	if (alot_count == 0 && a_lot_count == 0)
		return -1;

	double denominator = alot_count + a_lot_count;
	double numerator = alot_count + a_lot_count*2;

	return numerator/denominator;
}

double yeah_cond(const std::string& msg)
{
	int ya_count = 0;
	int yea_count = 0;
	int yeah_count = 0;

	using size_type = std::string::size_type;

	size_type result = 0;

	std::string mymsg = msg;

	std::transform(mymsg.begin(), mymsg.end(), mymsg.begin(), ::tolower);


	while ((result = mymsg.find("ya", result)) != std::string::npos)
	{
		++ya_count;
		++result;
	}

	result = 0;
	while ((result = mymsg.find("yea", result)) != std::string::npos)
	{
		if (result+3 == mymsg.length() ||
				(result+3 < mymsg.length() &&
				 mymsg[result+3] != 'h'))
			++yea_count;

		++result;
	}

	result = 0;
	while ((result = mymsg.find("yeah", result)) != std::string::npos)
	{
		++yeah_count;
		++result;
	}

	//Inconclusive:
	if (ya_count == 0 && yea_count == 0 && yeah_count == 0)
		return -1;

	double denominator = ya_count + yea_count + yeah_count;
	double numerator = ya_count + yea_count*2 + yeah_count*3;

	return numerator/denominator;
}

}
