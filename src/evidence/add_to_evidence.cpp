#include <iostream>
#include <string>
#include <sstream>
#include <list>
#include "sqlite3.h"
#include "evidence.h"
#include "sqlitedb.h"

using namespace std;

/* DO NOT MODIFY THIS CODE, CHANGES STILL TO BE MADE FOR USE IN RELEASE */

class db_entry
{
	/* message details and content */
	string personId; //senderID. receiverID is irrelevant.
	string msg; //message sent

	/* evidence integers */
	int punctuation=0;
	int emoticons=0;
	int period_space=0;
	int period_double_space=0;
	int first_capital_sentence=0;
	int first_capital_message=0;

public:

	db_entry(string newPersonID, string newMessage)
	{
		personId = newPersonID;
		msg = newMessage;
		findEvidenceFromMessage(); //no sense in not finding evidence right away
	}

	auto findEvidenceFromMessage() -> void
	{
		period_space = e::period_space(msg);
		period_double_space = e::period_double_space(msg);
		first_capital_sentence = e::first_capital_sentence(msg);
		first_capital_message = e::first_capital_message(msg);
		punctuation = e::punctuation(msg);
		emoticons = e::emoticons(msg);
	}

	auto getMaxEntryId(SQLiteDB& database) -> int //finds max ID (for forcing unique IDs elsewhere)
	{
		string selectQuery="SELECT id FROM evidence_master"; //select all id (eid)
		SQLiteStmt selectStmt = database.statement(selectQuery);
		//important note: can't use table or column names as parameters
		int rowSelect;
		int maxID=0;
		int foundID;

		rowSelect=selectStmt.step();
		while(rowSelect != SQLITE_DONE)
		{
			if(rowSelect == SQLITE_ROW)
			{
				foundID = selectStmt.column<int>(0);
				if(foundID >= maxID) //find biggest ID
				{
					maxID = foundID;
				}
			}
			else
			{
				cerr << "what happened" << endl;
				return -2;
			}
			rowSelect=selectStmt.step();
		}
		return maxID;
	}

	auto getPersonId() -> string
	{
		return personId;
	}
	auto getMessage() -> string
	{
		return msg;
	}

	/* Get Frequencies */
	auto getPeriodSingleSpace() -> int
	{
		return period_space;
	}
	auto getPeriodDoubleSpace() -> int
	{
		return period_double_space;
	}
	auto getFirstCapitalMessage() -> int
	{
		return first_capital_message;
	}
	auto getFirstCapitalSentence() -> int
	{
		return first_capital_sentence;
	}
	auto getPunctuation() -> int
	{
		return punctuation;
	}
	auto getEmoticons() -> int
	{
		return emoticons;
	}
	auto getEvidenceValues() -> vector<int>
	{
		return {period_space, period_double_space, first_capital_message, first_capital_sentence,
			punctuation, emoticons};
	}
}; //end of db_entry

auto insertIntoDatabase(SQLiteDB &dbEvidence, db_entry dbEntry) -> void
{
	string personId = dbEntry.getPersonId();
	string msg = dbEntry.getMessage();
	int nextID;
	string insertQuery = 
		"INSERT INTO evidence_master VALUES ( ?001 , ?002 , ?003 , ?004 , ?005 , NULL)";
	SQLiteStmt insertStmt = dbEvidence.statement(insertQuery);
	vector<int> values = dbEntry.getEvidenceValues(); //get all evidence values

	//forcing unique IDs since sqlite won't cooperate on this
	nextID = dbEntry.getMaxEntryId(dbEvidence) + 1;

	int eid=2; //id from evidences table, starts at 2

	for(int evalue : values)
	{
		insertStmt.bind(nextID++, 1);
		insertStmt.bind(personId, 2);
		insertStmt.bind(msg, 3);
		insertStmt.bind(eid++, 4); //use, then increment
		insertStmt.bind(evalue, 5);
		insertStmt.step(); //execute
		insertStmt.reset(); //reset query for rebinding
	}
}

auto createDatabaseEntry(SQLiteDB &evidenceDB, string personName, string messageContent) -> void
{
	db_entry dbEntry(personName, messageContent);
	insertIntoDatabase(evidenceDB, dbEntry);
}

int main()
{
	SQLiteDB evidenceDB("../../../db/evidences.db");
	createDatabaseEntry(evidenceDB, "bob", "This is the last test.  It's.");
}
