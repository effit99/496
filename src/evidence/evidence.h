#ifndef EVIDENCE_H
#define	EVIDENCE_H

#include <array>
#include <string>

namespace e{

struct Evidence
{
	Evidence(double(*fcn)(const std::string&), const std::string& description);
	Evidence(double (*fcn)(const std::string &), const std::string &description, bool cond);

	double(*const func)(const std::string&);
	const std::string name;
	std::size_t index;
	bool enabled;
	bool conditional;
private:
	static std::size_t maxIndex;
};

auto init() -> void;
auto first_capital_sentence(const std::string& msg) -> double;
auto first_capital_message(const std::string& msg) -> double;
//auto period_space(const std::string& msg) -> double;
//auto period_double_space(const std::string& msg) -> double;
auto avg_period_space(const std::string& msg) -> double; //Garranted till here
auto emoticons(const std::string& msg) -> double;
auto punctuation(const std::string& msg) -> double;
auto word_count(const std::string& msg) -> double;
auto avg_word_length(const std::string& msg) -> double;
auto length_chars(const std::string& msg) -> double;
auto length_words(const std::string& msg) -> double;
auto distinct_word_count(const std::string& msg) -> double;
auto distinct_char_count(const std::string& msg) -> double;
auto first_person_pronoun_count(const std::string& msg) -> double;
auto second_person_pronoun_count(const std::string& msg) -> double;
auto third_person_pronoun_count(const std::string& msg) -> double;
auto interogative_count(const std::string& msg) -> double;
auto negative_count(const std::string& msg) -> double;
auto positive_count(const std::string& msg) -> double;
auto apologetic_count(const std::string& msg) -> double;
auto capital_count(const std::string& msg) -> double;
auto lowercase_count(const std::string& msg) -> double;
auto exclamation_count(const std::string& msg) -> double;
auto question_mark_count(const std::string& msg) -> double;
auto comma_count(const std::string& msg) -> double;
auto space_count(const std::string& msg) -> double;
auto vowel_count(const std::string& msg) -> double;
auto consonant_count(const std::string& msg) -> double;
auto number_count(const std::string& msg) -> double;
auto message_period_end(const std::string& msg) -> double;

//conditional evidences
auto im_cond(const std::string& msg) -> double;
auto i_cond(const std::string& msg) -> double;
auto alot_cond(const std::string& msg) -> double;
auto yeah_cond(const std::string& msg) -> double;

extern auto set_evidence(const Evidence& e, const bool enabled) -> void;

//See evidence.cpp for explanation
constexpr int func_array_size =31;
using func_array_type = std::array<Evidence,func_array_size>;

extern func_array_type functions;


}

#endif	/* EVIDENCE_H */

